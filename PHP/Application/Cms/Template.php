<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Cms
 */
namespace Application\Cms;

use \Framework\Template\Exceptions as Exceptions;

/**
 * Provides methods to work with templates.
 * @access public
 */
class Template extends \Framework\Template\Template {

	/**
	 * Holds routes required to translate controller/action arrays of data into urls.
	 * // TODO: Think about some setter method with checking for isset, empty, is_array and array_key_exists
	 * @var array
	 * @access public
	 */
	public $Routes;

	/**
	 * Returns url for given controller/action name.
	 * // TODO: Fails sometimes with rules of equal number of elements which differ only in values other than controller and action
	 * @access public
	 * @param array $Data Array containing controller name, action name and other optional values.
	 * @uses \Framework\Template\Template::$Routes
	 * @uses \Framework\Template\Template::Clean()
	 * @return void
	 * @throws \Framework\Template\Exceptions\TemplateException if given argument wasn't non-empty array containing at least `Controller` index
	 * @see \Framework\Template\Template::Clean()
	 */
	public function Url($Data = array()) {
		if ((isset($Data)) && (is_array($Data)) && (!empty($Data))) {
			if (array_key_exists('Controller', $Data)) {
				foreach ($this->Routes as $Url => $Route) {
					if ($Route['Controller'] === $Data['Controller']) {
						if (array_keys($Data) == array_keys($Route)) {
							if (array_key_exists('Action', $Data)) {
								if ($Route['Action'] === $Data['Action']) {
									if ((count($Route) > 2) && ((count($Data) > 2)) && (count($Route) === count($Data))) {
										unset($Data['Controller'], $Data['Action']);
										foreach ($Data as $AdditionalKey => $AdditionalValue) {
											$Url = str_replace('[' . $AdditionalKey . ']', $AdditionalValue, $Url);
										}
										return $this->Clean($Url, $Data);
									}
									return $this->Clean($Url, $Data);
								}
							} else {
								return $this->Clean($Url, $Data);
							}
						}
					}
				}
				// Throwing an exception is not the best idea here, better create link to 404 page
				// TODO: Maybe better use /Default/. It's there for a reason...
				// throw new Exceptions\TemplateException('Could not match any url with data you\'ve passed');
				return $this->Clean('/^\/404$/');
			} else {
				throw new Exceptions\TemplateException('Array you\'ve passed does not contain `Controller` index');
			}
		} else {
			throw new Exceptions\TemplateException('You need to pass a non-empty array to ' . __CLASS__ . '::' . __FUNCTION__ . '()');
		}
	}

	/**
	 * Cleans url that's about to being returned
	 * You need to clean urls that come from {@see \Application\Cms\Template::Url()} before returning them to the template.
	 * This method strips escaped regexp characters and replaces value placeholders with their real values.
	 * @access private
	 * @param string $Url Url to be cleaned
	 * @param array $Data Array containing controller name, action name and so on
	 * @return string
	 * @see \Application\Cms\Template::Url()
	 */
	private function Clean($Url = '', $Data = array()) {
		$Url = str_replace('/^', '', $Url);
		$Url = str_replace('\/', '/', $Url);
		$Url = str_replace('$/', '', $Url);
		foreach ($Data as $Key => $Value) {
			$Url = str_replace('[' . $Key . ']', $Value, $Url);
		}
		return $Url;
	}
}
?>