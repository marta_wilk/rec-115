<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Cms
 */
namespace Application\Cms;

use \Framework\Bootstrap\WWWBootstrap;
use \Framework\ClassLoad\ClassLoader as ClassLoader;
use \Framework\ClassLoad\ClassLoaderManager;
use \Framework\Exception\ExceptionHandlerManager;
use \Framework\Exception\Handler as ExceptionHandler;
use \Framework\Error\ErrorHandlerManager;
use \Framework\Error\Handler as ErrorHandler;
use \Framework\Database\DatabaseConnectionManager;
use \Framework\Database\DatabaseConnection;
use \Framework\Database\DatabaseConnectionSettings;
// use \Framework\Template\Template;
use \Framework\Location\LocationManager;
use \Framework\Location\Location;
use \Framework\Misc\Meta;
use \Framework\Router\WWWRouter;

class Bootstrap extends WWWBootstrap {
	public $ClassLoaderManager;
	public $Config;
	public $ExceptionHandlerManager;
	public $ErrorHandlerManager;
	public $DatabaseConnectionManager;
	public $Database;
	public $Template;
	public $LocationManager;
	public $Meta;
	public $Router;

	public function __construct() {
		$this->ClassLoad();
		$this->Config();
		$this->Exception();
		$this->Error();
		$this->Database();
		$this->Template();
		$this->Location();
		$this->Meta();
		$this->Router();
	}

	private function ClassLoad() {
		$Filesystem = new ClassLoader\FilesystemClassLoader(
			array(
				'BasePath' => dirname(EF_DIR),
				'Path' => array(
					'Framework' . DS,
					'Application' . DS
				),
				'Recursive' => true,
				'Extension' => array(
					'.php',
					'.inc.php',
					'.class.php'
				)
			)
		);

		$Namespace = new ClassLoader\NamespaceClassLoader(
			array(
				'BasePath' => dirname(EF_DIR),
				'Extension' => array(
					'.php',
					'.inc.php',
					'.class.php'
				)
			)
		);

		$Multi = new ClassLoader\MultiClassLoader();
		$Multi->SetClassLoader('Namespace', $Namespace);
		$Multi->SetClassLoader('Filesystem', $Filesystem);

		$this->ClassLoaderManager = new ClassLoaderManager();
		$this->ClassLoaderManager->SetClassLoader('Multi', $Multi);
		$this->ClassLoaderManager->GetClassLoader('Multi')->RegisterClassLoader();
	}

	private function Config() {
		$this->Config = Config::GetStage();
	}

	private function Exception() {
		if (isset($this->Config['Exception']) && (!empty($this->Config['Exception'])) && (is_array($this->Config['Exception']))) {
			$this->ExceptionHandlerManager = new ExceptionHandlerManager();
			$this->ExceptionHandlerManager->SetExceptionHandler($this->Config['Exception']['HandlerName'], new $this->Config['Exception']['HandlerClass']());
		}
	}

	private function Error() {
		if (isset($this->Config['Error']) && (!empty($this->Config['Error'])) && (is_array($this->Config['Error']))) {
			$this->ErrorHandlerManager = new ErrorHandlerManager();
			$this->ErrorHandlerManager->SetErrorHandler($this->Config['Error']['HandlerName'], new $this->Config['Error']['HandlerClass']());
		}
	}

	private function Database() {
		if (isset($this->Config['Database']) && (!empty($this->Config['Database'])) && (is_array($this->Config['Database']))) {
			$this->DatabaseConnectionManager = new DatabaseConnectionManager();
			foreach ($this->Config['Database'] as $Name => $Database) {
				$ConnectionSettings = new DatabaseConnectionSettings();
				if (isset($Database['Host']))									$ConnectionSettings->SetHost($Database['Host']);
				if (isset($Database['Port']))									$ConnectionSettings->SetPort($Database['Port']);
				if (isset($Database['User']))									$ConnectionSettings->SetUser($Database['User']);
				if (isset($Database['Password']))								$ConnectionSettings->SetPassword($Database['Password']);
				if (isset($Database['Database']))								$ConnectionSettings->SetDatabase($Database['Database']);
				$Connection = new DatabaseConnection($ConnectionSettings, (isset($Database['AutoConnect'])) ? $Database['AutoConnect'] : true);
				$this->DatabaseConnectionManager->SetDatabaseConnection($Name, $Connection);
			}
			$this->Database = $this->DatabaseConnectionManager->GetDatabaseConnection('Root');
		}
	}

	private function Template() {
		$this->Template = new Template();
	}

	private function Location() {
		if (isset($this->Config['Location']) && (!empty($this->Config['Location'])) && (is_array($this->Config['Location']))) {
			$this->LocationManager = new LocationManager();
			foreach ($this->Config['Location'] as $Name => $Location) {
				$this->LocationManager->SetLocation($Name, new Location($Location['Protocol'], $Location['Host'], $Location['WebPath']));
			}
			$this->Template->Assign('LocationManager', $this->LocationManager);
		}
	}

	private function Meta() {
		if (isset($this->Config['Meta']) && (!empty($this->Config['Meta'])) && (is_array($this->Config['Meta']))) {
			$this->Meta = new Meta();
			if (isset($this->Config['Meta']['Title']))							$this->Meta->SetTitle($this->Config['Meta']['Title']);
			if (isset($this->Config['Meta']['Description']))					$this->Meta->SetDescription($this->Config['Meta']['Description']);
			if (isset($this->Config['Meta']['Keywords']))						$this->Meta->SetKeywords($this->Config['Meta']['Keywords']);
			if (isset($this->Config['Meta']['Canonical']))						$this->Meta->SetCanonical($this->Config['Meta']['Canonical']);
			$this->Template->Assign('Meta', $this->Meta);
		}
	}

	private function Router() {
		$Routes = array(
			'/^\/$/'								=> array('Controller' => '\Application\Cms\Contact\Index', 'Action' => 'Index'),

			'/^\/index$/'							=> array('Controller' => '\Application\Cms\Contact\Index', 'Action' => 'Index'),
			'/^\/add$/'								=> array('Controller' => '\Application\Cms\Contact\Index', 'Action' => 'Add'),
			'/^\/edit$/'							=> array('Controller' => '\Application\Cms\Contact\Index', 'Action' => 'Edit'),
			'/^\/delete-[ID]$/'						=> array('Controller' => '\Application\Cms\Contact\Index', 'Action' => 'Delete', 'ID' => '(?<ID>[0-9]+)'),
			'/^\/delete-all$/'						=> array('Controller' => '\Application\Cms\Contact\Index', 'Action' => 'DeleteAll'),

			'/^\/robots.txt$/'						=> array('Controller' => '\Application\Cms\System\Robots'),
			'/^\/sitemap.xml$/'						=> array('Controller' => '\Application\Cms\System\Sitemap'),
			'/Default/'								=> array('Controller' => '\Application\Cms\System\Error404'),
		);
		$this->Template->Routes = $Routes;
		$this->Router = new WWWRouter($this, $Routes);
		$this->Router->Route();
	}
}
?>