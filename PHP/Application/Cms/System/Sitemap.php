<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Cms
 */
namespace Application\Cms\System;

use \Framework\Root\RootObject;
use \Framework\Bootstrap\WWWBootstrap;

class Sitemap extends RootObject {
	public $Bootstrap;
	public $Template;

	public function __construct(WWWBootstrap $Bootstrap) {
		$this->Bootstrap = $Bootstrap;
		$this->Template = $this->Bootstrap->Template;
		$this->GetLinks();
		$this->Render();
	}

	private function GetLinks() {
		$Urls = array();
		$Urls[] = array('loc' => '/', 'lastmod' => date('Y-m-d'), 'changefreq' => 'daily', 'priority' => '1.0');
		$this->Template->Assign('Urls', $Urls);
	}

	private function Render() {
		header('Content-Type: text/xml');
		$this->Template->SetBasePath(EC_DIR . 'Template' . DS . 'System' . DS);
		$this->Template->Output('Sitemap.tpl.php');
	}
}
?>