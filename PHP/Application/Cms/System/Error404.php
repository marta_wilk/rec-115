<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Cms
 */
namespace Application\Cms\System;

use \Framework\Root\RootObject;
use \Framework\Bootstrap\WWWBootstrap;

class Error404 extends RootObject {
	public $Bootstrap;
	public $Template;
	public $LocationManager;
	public $Meta;

	public function __construct(WWWBootstrap $Bootstrap) {
		$this->Bootstrap = $Bootstrap;
		$this->Template = $this->Bootstrap->Template;
		$this->LocationManager = $this->Bootstrap->LocationManager;
		$this->Render();
	}

	private function Render() {
		header('HTTP/1.0 404 Not Found');
		$this->Template->SetBasePath(EC_DIR . 'Template' . DS . 'System' . DS);
		$this->Template->Output('Error404.tpl.php');
	}
}
?>