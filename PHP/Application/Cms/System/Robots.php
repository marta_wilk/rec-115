<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Cms
 */
namespace Application\Cms\System;

use \Framework\Root\RootObject;
use \Framework\Bootstrap\WWWBootstrap;

class Robots extends RootObject {
	public $Bootstrap;
	public $Template;
	public $LocationManager;
	public $Meta;

	public function __construct(WWWBootstrap $Bootstrap) {
		$this->Bootstrap = $Bootstrap;
		$this->Template = $this->Bootstrap->Template;
		$this->Render();
	}

	private function Render() {
		header('Content-Type: text/plain');
		$this->Template->SetBasePath(EC_DIR . 'Template' . DS . 'System' . DS);
		$this->Template->Output('Robots.tpl.php');
	}
}
?>