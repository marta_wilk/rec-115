<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Cms
 */
namespace Application\Cms;

class Config {
	public static $Stages = array(
		'Local' => array(
			'Host' => array(
				'php.localhost',
				'www.php.localhost'
			),
			'Exception' => array(
				'HandlerName' =>					'Html',
				'HandlerClass' =>					'\Framework\Exception\Handler\HtmlExceptionHandler'
			),
			'Error' => array(
				'HandlerName' =>					'Html',
				'HandlerClass' =>					'\Framework\Error\Handler\HtmlErrorHandler'
			),
			'Database' => array(
				'Root' => array(
					'Host' =>						'localhost',
					'Port' =>						3306,
					'User' =>						'entire',
					'Password' =>					'entire',
					'Database' =>					'php',
					'AutoConnect' =>				true
				)
			),
			'Location' => array(
				'Root' => array(
					'Protocol' =>					'http',
					'Host' =>						'php.localhost',
					'WebPath' =>					'/'
				),
				'Js' => array(
					'Protocol' =>					'http',
					'Host' =>						'php.localhost',
					'WebPath' =>					'/assets/js/'
				),
				'Css' => array(
					'Protocol' =>					'http',
					'Host' =>						'php.localhost',
					'WebPath' =>					'/assets/css/'
				),
				'Image' => array(
					'Protocol' =>					'http',
					'Host' =>						'php.localhost',
					'WebPath' =>					'/assets/img/'
				),
				'Font' => array(
					'Protocol' =>					'http',
					'Host' =>						'php.localhost',
					'WebPath' =>					'/assets/font/'
				)
			),
			'Meta' => array(
				'Title' =>							'Intelicom - PHP test',
				'Description' =>					'PHP test for Intelicom',
				'Keywords' =>						array('php', 'test', 'intelicom'),
				'Canonical' =>						'http://php.localhost/'
			),
			'Version' => array(
				'DIR' =>							EC_DIR,
				'NAME' =>							EC_NAME,
				'VERSION' =>						EC_VERSION,
				'WWW' =>							EC_WWW,
				'EMAIL' =>							EC_EMAIL,
				'DOC_URL' =>						EC_DOC_URL
			)
		)
	);

	public static function GetStage($Stage = '') {
		if ((isset($Stage)) && (!empty($Stage)) && (isset(self::$Stages[$Stage]))) {
			return self::$Stages[$Stage];
		}
		else {
			foreach (self::$Stages as $Stage) {
				if ((isset($Stage['Host'])) && (!empty($Stage['Host']))) {
					if (($Stage['Host'] == $_SERVER['HTTP_HOST']) || (in_array($_SERVER['HTTP_HOST'], $Stage['Host']))) {
						return $Stage;
					}
				}
			}
		}
	}
}
?>