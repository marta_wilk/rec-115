<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Cms
 */
namespace Application\Cms\Contact;

use \Framework\Root\RootException;

class Exception extends RootException {

}
?>