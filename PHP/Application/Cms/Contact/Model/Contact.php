<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Cms
 */
namespace Application\Cms\Contact\Model;

use \Framework\Root\MVC\Model;

class Contact extends Model {
	protected $TableName = 'contact';
	protected $PrimaryKey = 'id';

	protected $SortFields = array(
		'id',
		'first_name',
		'last_name',
		'phone',
		'email',
		'address',
		'city',
		'zip',
		'is_friend'
	);
	protected $SortDirections = array(
		'asc',
		'desc'
	);

	protected $SortField;
	protected $SortDirection;

	public function __construct($DatabaseConnection) {
		parent::__construct($DatabaseConnection);
		$this->SetSort($this->SortFields[0], $this->SortDirections[0]);
	}

	public function SetSort($Field, $Direction) {
		if ((isset($Field)) && (!empty($Field)) && (in_array($Field, $this->SortFields))) {
			$this->SortField = $Field;
		}
		else {
			$this->SortField = $this->SortFields[0];
		}

		if ((isset($Direction)) && (!empty($Direction)) && (in_array($Direction, $this->SortDirections))) {
			$this->SortDirection = $Direction;
		}
		else {
			$this->SortDirection = $this->SortDirections[0];
		}
	}

	public function GetSort() {
		$Sort = new \stdClass();
		$Sort->field = $this->SortField;
		$Sort->direction = $this->SortDirection;
		return $Sort;
	}

	public function GetContacts() {
		$Sort = $this->GetSort();
		$SQL = "SELECT * FROM `" . $this->TableName . "` ORDER BY `" . $Sort->field . "` " . strtoupper($Sort->direction);
		$Query = $this->DatabaseConnection->Query($SQL);
		$Result = $this->DatabaseConnection->{$this->MultipleResultsFunction}($Query);
		return $Result;
	}

	public function GetTotal() {
		$SQL = "SELECT COUNT(*) as `count` FROM `" . $this->TableName . "`";
		$Query = $this->DatabaseConnection->Query($SQL);
		$Result = $this->DatabaseConnection->{$this->SingleResultFunction}($Query);
		return $Result->count;
	}

	public function GetFriends() {
		$SQL = "SELECT COUNT(*) as `count` FROM `" . $this->TableName . "` WHERE `is_friend` = 'Y'";
		$Query = $this->DatabaseConnection->Query($SQL);
		$Result = $this->DatabaseConnection->{$this->SingleResultFunction}($Query);
		return $Result->count;
	}

	public function Delete($ID) {
		$SQL = "DELETE FROM `" . $this->TableName . "` WHERE `" . $this->PrimaryKey . "` = '" . $ID . "' LIMIT 1";
		$this->DatabaseConnection->Query($SQL);
	}

	public function DeleteAll() {
		$SQL = "DELETE FROM `" . $this->TableName . "`";
		$this->DatabaseConnection->Query($SQL);
	}

	public function Add($FirstName, $LastName, $Phone, $Email, $Address, $City, $Zip, $IsFriend) {
		$FirstName = mysql_real_escape_string($FirstName);
		$LastName = mysql_real_escape_string($LastName );
		$Phone = mysql_real_escape_string($Phone);
		$Email = mysql_real_escape_string($Email);
		$Address = mysql_real_escape_string($Address);
		$City = mysql_real_escape_string($City);
		$Zip = mysql_real_escape_string($Zip);
		$IsFriend = mysql_real_escape_string($IsFriend);

		$SQL = "INSERT INTO `" . $this->TableName . "` (`first_name`, `last_name`, `phone`, `email`, `address`, `city`, `zip`, `is_friend`) VALUES ('" . $FirstName . "', '" . $LastName . "', '" . $Phone . "', '" . $Email . "', '" . $Address . "', '" . $City . "', '" . $Zip . "', '" . $IsFriend . "')";
		$this->DatabaseConnection->Query($SQL);
	}
}
?>