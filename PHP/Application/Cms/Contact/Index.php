<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Cms
 */
namespace Application\Cms\Contact;

use \Framework\Root\MVC\Controller;
use \Framework\Bootstrap\WWWBootstrap;
use \Framework\Location\LocationManager;
use \Framework\Location\Location;
use \Framework\Misc\Meta;
use \Framework\Misc\Misc;
use \Application\Cms\Contact\Model\Contact as ContactModel;

class Index extends Controller {
	public $Bootstrap;
	public $Database;
	public $Template;
	public $LocationManager;
	public $Meta;
	public $Request;

	public function __construct(WWWBootstrap $Bootstrap, $Request) {
		$this->Bootstrap = $Bootstrap;
		$this->Database = $this->Bootstrap->Database;
		$this->Template = $this->Bootstrap->Template;
		$this->LocationManager = $this->Bootstrap->LocationManager;
		$this->Meta = $this->Bootstrap->Meta;
		$this->Request = $Request;
		if ((isset($Request)) && (!empty($Request)) && (is_array($Request))) {
			if ((isset($Request['Action'])) && (!empty($Request['Action']))) {
				switch($Request['Action']) {
					case 'Index':					$this->Index($Request); break;
					case 'Delete':					$this->Delete($Request); break;
					case 'DeleteAll':				$this->DeleteAll($Request); break;
					case 'Add':						$this->Add($Request); break;
					default:						throw new Exception('Action not allowed: ' . $Request['Action']);
				}
			}
			else {
				throw new Exception('Action is required.');
			}
		}
	}

	private function Index($Request) {
		$Model = new ContactModel($this->Database);

		$Model->SetSort(@$Request['sort_field'], @$Request['sort_direction']);
		$this->Template->Assign('Sort', $Model->GetSort());


		$this->Template->Assign('Contacts', $Model->GetContacts());

		$Summary = new \stdClass();
		$Summary->total = $Model->GetTotal();
		$Summary->friends = $Model->GetFriends();
		$this->Template->Assign('Summary', $Summary);

		$this->Render(__FUNCTION__);
	}

	private function Delete($Request) {
		$Model = new ContactModel($this->Database);
		$Model->Delete($Request['ID']);
		Misc::GetInstance()->Redirect($this->Template->Url(array('Controller' => '\Application\Cms\Contact\Index', 'Action' => 'Index')));
	}

	private function DeleteAll($Request) {
		$Model = new ContactModel($this->Database);
		$Model->DeleteAll();
		Misc::GetInstance()->Redirect($this->Template->Url(array('Controller' => '\Application\Cms\Contact\Index', 'Action' => 'Index')));
	}

	private function Add($Request) {
		$Model = new ContactModel($this->Database);
		$Model->Add(@$Request['first_name'], @$Request['last_name'], @$Request['phone'], @$Request['email'], @$Request['address'], @$Request['city'], @$Request['zip'], @$Request['is_friend']);
		Misc::GetInstance()->Redirect($this->Template->Url(array('Controller' => '\Application\Cms\Contact\Index', 'Action' => 'Index')));
	}

	private function Render($Page, $Layout = 'Root.tpl.php') {
		$this->Template->Assign('Page', 'Contact' . DS . $Page);
		$this->Template->SetBasePath(EC_DIR . 'Template' . DS);
		$this->Template->Output($Layout);
	}
}
?>