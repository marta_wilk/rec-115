<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Cms
 */
if (!defined('EC_DIR'))
	/**
	 * Root directory of EC.
	 */
	define('EC_DIR', dirname(__FILE__) . DS);

if (!defined('EC_NAME'))
	/**
	 * Name of EC.
	 */
	define('EC_NAME', 'EntireCms');

if (!defined('EC_VERSION'))
	/**
	 * Version of EC.
	 */
	define('EC_VERSION', '0.7.0');

if (!defined('EC_WWW'))
	/**
	 * Url(s) of EC.
	 */
	define('EC_WWW', '');

if (!defined('EC_EMAIL'))
	/**
	 * E-mail address(es) of EC.
	 */
	define('EC_EMAIL', '');

if (!defined('EC_DOC_URL'))
	/**
	 * Documentation url of EC.
	 */
	define('EC_DOC_URL', '');
?>