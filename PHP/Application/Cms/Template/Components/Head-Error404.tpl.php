<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $Meta->GetTitle(); ?></title>
	<meta name="description" content="<?php echo $Meta->GetDescription(); ?>">
	<meta name="keywords" content="<?php echo $Meta->GetKeywords(); ?>">
	<meta name="robots" content="noindex,nofollow">
	<meta name="googlebot" content="noindex,nofollow">
	<link rel="canonical" href="<?php echo $Meta->GetCanonical(); ?>">
	<link rel="shortcut icon" type="image/x-icon" href="<?php if ((isset($LocationManager)) && (!empty($LocationManager))) echo $LocationManager->GetLocation('Root')->toString(); ?>favicon.ico?v=<?php echo EC_VERSION; ?>">
	<link rel="icon" type="image/x-icon" href="<?php if ((isset($LocationManager)) && (!empty($LocationManager))) echo $LocationManager->GetLocation('Root')->toString(); ?>favicon.ico?v=<?php echo EC_VERSION; ?>">
	<link rel="stylesheet" type="text/css" href="<?php if ((isset($LocationManager)) && (!empty($LocationManager))) echo $LocationManager->GetLocation('Css')->toString(); ?>404.css?v=<?php echo EC_VERSION; ?>">
</head>