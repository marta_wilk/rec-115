<div class="container">
	<hr>
	<a href="#" class="btn btn-primary pull-right add-contact">Add contact</a>
	<?php if ((isset($Contacts)) && (!empty($Contacts))): ?>
		<a class="btn btn-default" href="<?php echo $this->Url(array('Controller' => '\Application\Cms\Contact\Index', 'Action' => 'DeleteAll')); ?>">Remove All</a>
	<?php endif; ?>
</div>