<!DOCTYPE html>
<html>
	<?php $this->Import('../Components/Head-Error404.tpl.php'); ?>
	<body class="Error404">
		<div class="container">
			<h1>Error 404</h1>
			<h3>Page not found</h3>
			<h5><a href="<?php if ((isset($LocationManager)) && (!empty($LocationManager))) echo $LocationManager->GetLocation('Root')->toString(); ?>">&laquo; back to homepage</a></h5>
		</div>
	</body>
</html>