<?php
echo '<?xml version="1.0" encoding="UTF-8" ?>' . "\n";
echo '<?xml-stylesheet type="text/css" href="assets/css/sitemap.css" ?>' . "\n";
echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

foreach ($Urls as $Url) {
	echo '
	<url>
		<loc>' . (((isset($LocationManager)) && (!empty($LocationManager))) ? $LocationManager->GetLocation('Root')->toString() . ltrim($Url['loc'], '/') : ltrim($Url['loc'], '/')) . '</loc>
		<lastmod>' . date('Y-m-d', strtotime($Url['lastmod'])) . '</lastmod>
		<changefreq>' . $Url['changefreq'] . '</changefreq>
		<priority>' . number_format($Url['priority'], 2, '.', '') . '</priority>
	</url>';
}

echo "\n" . '</urlset>';
?>