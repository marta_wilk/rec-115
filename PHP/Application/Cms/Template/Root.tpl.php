<!DOCTYPE html>
<html>
	<?php $this->Import('Components/Head.tpl.php'); ?>
	<body <?php echo ((isset($Page)) && (!empty($Page))) ? 'class="' . str_replace('\\', '', $Page) . '"' : ''; ?>>
		<?php $this->Import('Components/Header.tpl.php'); ?>
		<div class="container">
			<?php if ((isset($Page)) && (!empty($Page))) $this->Import($Page . '.tpl.php'); ?>
		</div>
		<?php $this->Import('Components/Foot.tpl.php'); ?>
		<?php $this->Import('Contact/Add.tpl.php'); ?>
		<?php $this->Import('Components/Footer.tpl.php'); ?>
	</body>
</html>