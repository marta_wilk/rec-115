<?php if ((isset($Contacts)) && (!empty($Contacts))): ?>
	<table class="table table-hover">
		<thead>
			<tr>
				<th><a href="?sort_field=id&sort_direction=<?php echo ($Sort->direction === 'asc') ? 'desc' : 'asc'; ?>"><span class="fa fa-sort<?php echo ($Sort->field === 'id') ? '-' . $Sort->direction : ''; ?>"></span>#</a></th>
				<th><a href="?sort_field=first_name&sort_direction=<?php echo ($Sort->direction === 'asc') ? 'desc' : 'asc'; ?>"><span class="fa fa-sort<?php echo ($Sort->field === 'first_name') ? '-' . $Sort->direction : ''; ?>"></span>First name</a></th>
				<th><a href="?sort_field=last_name&sort_direction=<?php echo ($Sort->direction === 'asc') ? 'desc' : 'asc'; ?>"><span class="fa fa-sort<?php echo ($Sort->field === 'last_name') ? '-' . $Sort->direction : ''; ?>"></span>Last name</a></th>
				<th><a href="?sort_field=phone&sort_direction=<?php echo ($Sort->direction === 'asc') ? 'desc' : 'asc'; ?>"><span class="fa fa-sort<?php echo ($Sort->field === 'phone') ? '-' . $Sort->direction : ''; ?>"></span>Phone number</a></th>
				<th><a href="?sort_field=email&sort_direction=<?php echo ($Sort->direction === 'asc') ? 'desc' : 'asc'; ?>"><span class="fa fa-sort<?php echo ($Sort->field === 'email') ? '-' . $Sort->direction : ''; ?>"></span>Email</a></th>
				<th><a href="?sort_field=address&sort_direction=<?php echo ($Sort->direction === 'asc') ? 'desc' : 'asc'; ?>"><span class="fa fa-sort<?php echo ($Sort->field === 'address') ? '-' . $Sort->direction : ''; ?>"></span>Address</a></th>
				<th><a href="?sort_field=city&sort_direction=<?php echo ($Sort->direction === 'asc') ? 'desc' : 'asc'; ?>"><span class="fa fa-sort<?php echo ($Sort->field === 'city') ? '-' . $Sort->direction : ''; ?>"></span>City</a></th>
				<th><a href="?sort_field=zip&sort_direction=<?php echo ($Sort->direction === 'asc') ? 'desc' : 'asc'; ?>"><span class="fa fa-sort<?php echo ($Sort->field === 'zip') ? '-' . $Sort->direction : ''; ?>"></span>Zip</a></th>
				<th><a href="?sort_field=is_friend&sort_direction=<?php echo ($Sort->direction === 'asc') ? 'desc' : 'asc'; ?>"><span class="fa fa-sort<?php echo ($Sort->field === 'is_friend') ? '-' . $Sort->direction : ''; ?>"></span>Is friend ?</a></th>
				<th>&nbsp;</th>
			</tr>
		</thead>
			<tbody>
			<?php foreach ($Contacts as $Contact): ?>
				<tr>
					<th><?php echo $Contact->id; ?></th>
					<th><?php echo $Contact->first_name; ?></th>
					<th><?php echo $Contact->last_name; ?></th>
					<th><?php echo $Contact->phone; ?></th>
					<th><a href="mailto:<?php echo $Contact->email; ?>"><?php echo $Contact->email; ?></a></th>
					<th><?php echo $Contact->address; ?></th>
					<th><?php echo $Contact->city; ?></th>
					<th><?php echo $Contact->zip; ?></th>
					<th><?php echo ($Contact->is_friend === 'Y') ? '<span class="fa fa-check"></span>' : '<span class="fa fa-close"></span>'; ?></th>
					<th>
						<a class="btn btn-primary" href="<?php echo $this->Url(array('Controller' => '\Application\Cms\Contact\Index', 'Action' => 'Delete', 'ID' => $Contact->id)); ?>">Remove</a>
					</th>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<hr>
	<p>
		<strong>Total:</strong> <?php echo $Summary->total; ?><br>
		<strong>Friends:</strong> <?php echo $Summary->friends; ?>
	</p>
<?php else: ?>
	<p>There is no contacts yet!</p>
<?php endif; ?>