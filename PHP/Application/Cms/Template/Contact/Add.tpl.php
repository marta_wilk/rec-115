<div id="Overlay"></div>
<div id="Add">
	<h1>Add contact</h1>
	<form action="<?php echo $this->Url(array('Controller' => '\Application\Cms\Contact\Index', 'Action' => 'Add')); ?>" method="post">
			<div class="form-group">
				<label for="first_name">First name:</label>
				<input type="text" id="first_name" name="first_name" class="form-control">
			</div>
			<div class="form-group">
				<label for="last_name">Last name:</label>
				<input type="text" id="last_name" name="last_name" class="form-control">
			</div>
			<div class="form-group">
				<label for="zip">Phone:</label>
				<input type="text" id="phone" name="phone" class="form-control">
			</div>
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="text" id="email" name="email" class="form-control">
			</div>
			<div class="form-group">
				<label for="address">Address:</label>
				<input type="text" id="address" name="address" class="form-control">
			</div>
			<div class="form-group">
				<label for="city">City:</label>
				<input type="text" id="city" name="city" class="form-control">
			</div>
			<div class="form-group">
				<label for="zip">Zip:</label>
				<input type="text" id="zip" name="zip" class="form-control">
			</div>
			<div class="form-group">
				<label for="is_friend">Is friend:</label>
				<select id="is_friend" name="is_friend" class="form-control">
					<option value="Y">Yes</option>
					<option value="N">No</option>
				</select>
			</div>
			<input type="submit" class="btn btn-primary" value="Add">
	</form>
</div>