DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(10) unsigned NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip` varchar(6) NOT NULL,
  `is_friend` enum('Y','N') NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `contact` (`id`, `first_name`, `last_name`, `phone`, `email`, `address`, `city`, `zip`, `is_friend`) VALUES
(1, 'Tomasz', 'Jóźwik', '+48 505 106 370', 'entire@entire.pl', 'ul. Marii Skłodowskiej-Curie 9', 'Brańsk', '17-102', 'Y'),
(2, 'Adam', 'Kasabuła', '+48 123 456 798', 'adam.kasabula@entire.pl', 'ul. Zagórna 18/4', 'Białystok', '15-506', 'N');

ALTER TABLE `contact`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `contact`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
