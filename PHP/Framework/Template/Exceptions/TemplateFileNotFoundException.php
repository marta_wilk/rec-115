<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Template\Exceptions
 */
namespace Framework\Template\Exceptions;

/**
 * File not found template exception
 * @access public
 */
class TemplateFileNotFoundException extends TemplateException {

}
?>