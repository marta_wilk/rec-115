<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Template\Exceptions
 */
namespace Framework\Template\Exceptions;

/**
 * Base path related template exception
 * @access public
 */
class TemplateBasePathException extends TemplateException {

}
?>