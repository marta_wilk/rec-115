<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Template\Exceptions
 */
namespace Framework\Template\Exceptions;

use \Framework\Root\RootException;

/**
 * Common ancestor for all template related exceptions
 * @access public
 */
class TemplateException extends RootException {

}
?>