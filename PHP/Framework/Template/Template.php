<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Template
 */
namespace Framework\Template;

use \Framework\Root\RootObject;
use \Framework\Template\Exceptions as Exceptions;

/**
 * Provides methods to work with templates.
 * @access public
 */
class Template extends RootObject {

	/**
	 * Holds path to template directory.
	 * @var string
	 * @access private
	 */
	private $TemplateBasePath;

	/**
	 * Holds template content.
	 * @var string
	 * @access private
	 */
	private $TemplateBuffer;

	/**
	 * Associative array holding names and values of variables assigned to template.
	 * @var array
	 * @access private
	 */
	private $TemplateVars = array();

	/**
	 * Sets base path.
	 * Sets path to directory containing templates and returns true or throws an exception if given string was empty or directory was unreadable.
	 * @access public
	 * @param string $_Path Path to directory containing templates
	 * @uses \Framework\Template\Template::$TemplateBasePath
	 * @return bool
	 * @throws \Framework\Template\Exceptions\TemplateBasePathException if given string was empty or directory was unreadable.
	 */
	public function SetBasePath($_Path) {
		if ((isset($_Path)) && (!empty($_Path)) && (is_readable($_Path))) {
			$_Path = rtrim($_Path, DS) . DS;
			$this->TemplateBasePath = $_Path;
			return true;
		}
		else {
			throw new Exceptions\TemplateBasePathException('Specified template base path is empty or unreadable: ' . $_Path);
		}
	}

	/**
	 * Gets base path used.
	 * Returns previously set base path or throws an exception if base path was not set before.
	 * @access public
	 * @uses \Framework\Template\Template::$TemplateBasePath
	 * @return string
	 * @throws \Framework\Template\Exceptions\TemplateBasePathException if base path was not set before.
	 */
	public function GetBasePath() {
		if ((isset($this->TemplateBasePath)) && (is_string($this->TemplateBasePath)) && (!empty($this->TemplateBasePath))) {
			return $this->TemplateBasePath;
		}
		else {
			throw new Exceptions\TemplateBasePathException('Base path is empty.');
		}
	}

	/**
	 * Sets variable to be used by template.
	 * Sets variable for template. It will overwrite any previously set variable with the same name.
	 * @access public
	 * @param string $_Variable Name of variable
	 * @param mixed $_Value Value of variable
	 * @uses \Framework\Template\Template::$TemplateVars
	 * @return void
	 */
	public function Assign($_Variable, $_Value) {
		$this->TemplateVars[(string) $_Variable] = $_Value;
	}

	/**
	 * Imports template within another template.
	 * You can nest your templates endlessly with this method. Just call it from one template and load another one.
	 * It will throw an exception if template could not be found or given template file name is empty.
	 * @access public
	 * @param string $_Template File name of template to be loaded
	 * @uses \Framework\Template\Template::$TemplateBasePath
	 * @uses \Framework\Template\Template::$TemplateVars
	 * @return void
	 * @throws \Framework\Template\Exceptions\TemplateBasePathException if given template file name is empty.
	 * @throws \Framework\Template\Exceptions\TemplateFileNotFoundException if given template could not be found.
	 * @see \Framework\Template\Template::Output()
	 * @see \Framework\Template\Template::Fetch()
	 */
	public function Import($_Template) {
		if ((isset($_Template)) && (!empty($_Template))) {
			if (file_exists($this->TemplateBasePath . $_Template)) {
				extract($this->TemplateVars, EXTR_SKIP);
				require $this->TemplateBasePath . $_Template;
			}
			else {
				throw new Exceptions\TemplateFileNotFoundException('Template file could not be found: ' . $_Template);
			}
		}
		else {
			throw new Exceptions\TemplateException('Template file is empty.');
		}
	}

	/**
	 * Outputs template to web browser.
	 * Prints content of given template to user's web browser.
	 * It will throw an exception if template could not be found or given template file name is empty.
	 * @access public
	 * @param string $_Template File name of template to be loaded
	 * @uses \Framework\Template\Template::$TemplateBasePath
	 * @uses \Framework\Template\Template::$TemplateBuffer
	 * @uses \Framework\Template\Template::$TemplateVars
	 * @return void
	 * @throws \Framework\Template\Exceptions\TemplateBasePathException if given template file name is empty.
	 * @throws \Framework\Template\Exceptions\TemplateFileNotFoundException if given template could not be found.
	 * @see \Framework\Template\Template::Import()
	 * @see \Framework\Template\Template::Fetch()
	 */
	public function Output($_Template) {
		if ((isset($_Template)) && (!empty($_Template))) {
			if (file_exists($this->TemplateBasePath . $_Template)) {
				ob_start();
				//---
				extract($this->TemplateVars, EXTR_SKIP);
				require $this->TemplateBasePath . $_Template;
				//---
				$this->TemplateBuffer = ob_get_contents();
				ob_end_clean();
			}
			else {
				throw new Exceptions\TemplateFileNotFoundException('Template file could not be found: ' . $_Template);
			}
		}
		else {
			throw new Exceptions\TemplateException('Template file is empty.');
		}
	}

	/**
	 * Fetches given template.
	 * Returns content of given template.
	 * It will throw an exception if template could not be found or given template file name is empty.
	 * @access public
	 * @param string $_Template File name of template to be loaded
	 * @uses \Framework\Template\Template::$TemplateBasePath
	 * @uses \Framework\Template\Template::$TemplateVars
	 * @return string
	 * @throws \Framework\Template\Exceptions\TemplateBasePathException if given template file name is empty.
	 * @throws \Framework\Template\Exceptions\TemplateFileNotFoundException if given template could not be found.
	 * @see \Framework\Template\Template::Import()
	 * @see \Framework\Template\Template::Output()
	 */
	public function Fetch($_Template) {
		if ((isset($_Template)) && (!empty($_Template))) {
			if (file_exists($this->TemplateBasePath . $_Template)) {
				ob_start();
				//---
				extract($this->TemplateVars, EXTR_SKIP);
				require $this->TemplateBasePath . $_Template;
				//---
				$_Buffer = ob_get_contents();
				ob_end_clean();
				return $_Buffer;
			}
			else {
				throw new Exceptions\TemplateFileNotFoundException('Template file could not be found: ' . $_Template);
			}
		}
		else {
			throw new Exceptions\TemplateException('Template file is empty.');
		}
	}

	/**
	 * Echoes content of {@see \Framework\Template\Template::$TemplateBuffer}
	 * @access public
	 * @uses \Framework\Template\Template::$TemplateBuffer
	 * @return void
	 */
	public function __destruct() {
		echo $this->TemplateBuffer;
	}
}
?>