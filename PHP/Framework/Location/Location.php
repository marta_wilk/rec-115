<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Location
 */
namespace Framework\Location;

use \Framework\Root\RootObject;
use \Framework\Location\Exceptions as Exceptions;

/**
 * Provides methods to validate and encapsulate web locations which consist of protocol, host (optionally with port) and (optional) web path.
 */
class Location extends RootObject {

	/**
	 * Holds protocol being used.
	 * @var string
	 * @access private
	 */
	private $Protocol;

	/**
	 * Holds host being used.
	 * @var string
	 * @access private
	 */
	private $Host;

	/**
	 * Holds web path being used.
	 * @var string
	 * @access private
	 */
	private $WebPath;

	/**
	 * Creates instance of location class.
	 * If 3 parameters are passed, each of them is fed to {@link \Framework\Location\Location::SetProtocol()}, {@link \Framework\Location\Location::SetHost()} and {@link \Framework\Location\Location::SetWebPath()} respectively.
	 * In case there are only 2 parameters passed - {@link \Framework\Location\Location::SetWebPath()} is fed with /.
	 * Otherwise, does not call any methods and simply returns {@link \Framework\Location\Location} instance
	 * @access public
	 * @param string $Protocol (Optional)
	 * @param string $Host (Optional)
	 * @param string $WebPath (Optional)
	 * @uses \Framework\Location\Location::SetProtocol()
	 * @uses \Framework\Location\Location::SetHost()
	 * @uses \Framework\Location\Location::SetWebPath()
	 * @return \Framework\Location\Location
	 */
	public function __construct() {
		if (func_num_args() == 3) {
			$this->SetProtocol(func_get_arg(0));
			$this->SetHost(func_get_arg(1));
			$this->SetWebPath(func_get_arg(2));
		}
		else if (func_num_args() == 2) {
			$this->SetProtocol(func_get_arg(0));
			$this->SetHost(func_get_arg(1));
			$this->SetWebPath('/');
		}
	}

	/**
	 * Returns location string.
	 * Returns stringified location, for example: http://www.example.com/directory/
	 * @access public
	 * @uses \Framework\Location\Location::GetProtocol()
	 * @uses \Framework\Location\Location::GetHost()
	 * @uses \Framework\Location\Location::GetWebPath()
	 * @return string
	 */
	public function toString() {
		return $this->GetProtocol() . '://' . $this->GetHost() . $this->GetWebPath();
	}

	/**
	 * Sets protocol to use.
	 * Sets protocol to one from following: 'http', 'https', 'ftp', 'ftps' and returns true or throws an exception if given value was invalid.
	 * @access public
	 * @param string $Protocol One of 'http', 'https', 'ftp', 'ftps'
	 * @uses \Framework\Location\Location::$Protocol
	 * @return bool
	 * @throws \Framework\Location\Exceptions\LocationProtocolException if given value was invalid.
	 * @see \Framework\Location\Location::GetProtocol()
	 */
	public function SetProtocol($Protocol) {
		if ((isset($Protocol)) && (is_string($Protocol)) && (!empty($Protocol))) {
			if (in_array($Protocol, array('http', 'https', 'ftp', 'ftps'))) {
				$this->Protocol = $Protocol;
				return true;
			}
			else {
				throw new Exceptions\LocationProtocolException('Given protocol is not supported: ' . $Protocol);
			}
		}
		else {
			throw new Exceptions\LocationProtocolException('Given protocol is empty.');
		}
	}

	/**
	 * Gets protocol used.
	 * Returns previously set protocol or throws an exception if protocol was not set before.
	 * @access public
	 * @uses \Framework\Location\Location::$Protocol
	 * @return string
	 * @throws \Framework\Location\Exceptions\LocationProtocolException if protocol was not set before.
	 * @see \Framework\Location\Location::SetProtocol()
	 */
	public function GetProtocol() {
		if ((isset($this->Protocol)) && (is_string($this->Protocol)) && (!empty($this->Protocol))) {
			return $this->Protocol;
		}
		else {
			throw new Exceptions\LocationProtocolException('Protocol is empty.');
		}
	}

	/**
	 * Sets host to use.
	 * Sets host to use which has to be valid domain or IP address, optionally with port. Returns true or throws an exception if given value was invalid.
	 * @access public
	 * @param string $Host Valid domain or IP address, optionally with port
	 * @uses \Framework\Location\Location::$Host
	 * @return bool
	 * @throws \Framework\Location\Exceptions\LocationHostException if given value was invalid.
	 * @see \Framework\Location\Location::GetHost()
	 */
	public function SetHost($Host) {
		if ((isset($Host)) && (is_string($Host)) && (!empty($Host))) {
			$Pattern = (preg_match('#^(?:[0-9.:]+)$#', $Host, $Matches)) ? '#^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(?::[0-9]{1,5})?$#' : '#^((?:[a-z0-9][a-z0-9-]+(?<!-)\.)((?:(?!-)[a-z0-9-]+\.)+)?(?:(?<!-)(?!-)[a-z0-9]{1,20}))(?::[0-9]{1,5})?$|^(?:localhost)(?::[0-9]{1,5})?$#i';
			if (preg_match($Pattern, $Host, $Matches)) {
				$this->Host = $Host;
				return true;
			}
			else {
				throw new Exceptions\LocationHostException('Given host is invalid: ' . $Host);
			}
		}
		else {
			throw new Exceptions\LocationHostException('Given host is empty.');
		}
	}

	/**
	 * Gets host used.
	 * Returns previously set host or throws an exception if host was not set before.
	 * @access public
	 * @uses \Framework\Location\Location::$Host
	 * @return string
	 * @throws \Framework\Location\Exception\LocationHostException if host was not set before.
	 * @see \Framework\Location\Location::SetHost()
	 */
	public function GetHost() {
		if ((isset($this->Host)) && (is_string($this->Host)) && (!empty($this->Host))) {
			return $this->Host;
		}
		else {
			throw new Exceptions\LocationHostException('Host is empty.');
		}
	}

	/**
	 * Sets web path to use.
	 * Sets web path to use (no validation is being done). Returns true if web path is not empty or throws an exception if given value was invalid.
	 * @access public
	 * @param string $WebPath
	 * @uses \Framework\Location\Location::$WebPath
	 * @return bool
	 * @throws \Framework\Location\Exceptions\LocationWebPathException if given value was invalid.
	 * @see \Framework\Location\Location::GetWebPath()
	 */
	public function SetWebPath($WebPath) {
		if ((isset($WebPath)) && (is_string($WebPath)) && (!empty($WebPath))) {
			$WebPath = '/' . \ltrim($WebPath, '/');
			$WebPath = \rtrim($WebPath, '/') . '/';
			$this->WebPath = $WebPath;
			return true;
		}
		else {
			throw new Exceptions\LocationWebPathException('Given web path is empty.');
		}
	}

	/**
	 * Gets web path used.
	 * Returns previously set web path or throws an exception if web path was not set before.
	 * @access public
	 * @uses \Framework\Location\Location::$WebPath
	 * @return string
	 * @throws \Framework\Location\Exceptions\LocationWebPathException if web path was not set before.
	 * @see \Framework\Location\Location::SetWebPath()
	 */
	public function GetWebPath() {
		if ((isset($this->WebPath)) && (is_string($this->WebPath)) && (!empty($this->WebPath))) {
			return $this->WebPath;
		}
		else {
			throw new Exceptions\LocationWebPathException('Web path is empty.');
		}
	}
}
?>