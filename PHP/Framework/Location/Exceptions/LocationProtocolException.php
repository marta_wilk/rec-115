<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Location
 */
namespace Framework\Location\Exceptions;

/**
 * Protocol related location exception
 */
class LocationProtocolException extends LocationException {

}
?>