<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Location
 */
namespace Framework\Location\Exceptions;

use \Framework\Root\RootException;

/**
 * Location manager related exception
 */
class LocationManagerException extends RootException {

}
?>