<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Location
 */
namespace Framework\Location\Exceptions;

/**
 * Web path related location exception
 */
class LocationWebPathException extends LocationException {

}
?>