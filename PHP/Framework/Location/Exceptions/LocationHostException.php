<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Location
 */
namespace Framework\Location\Exceptions;

/**
 * Host related location exception
 */
class LocationHostException extends LocationException {

}
?>