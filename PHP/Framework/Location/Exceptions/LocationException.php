<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Location
 */
namespace Framework\Location\Exceptions;

use \Framework\Root\RootException;

/**
 * Common ancestor for all location related exceptions
 */
class LocationException extends RootException {

}
?>