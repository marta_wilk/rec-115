<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Location
 */
namespace Framework\Location;

use \Framework\Root\RootObject;
use \Framework\Location\Exceptions as Exceptions;

/**
 * Provides methods to aggregate location instances
 */
class LocationManager extends RootObject {

	/**
	 * Associative array of locations
	 * @var array
	 * @access private
	 */
	private $Location;

	/**
	 * Holds current index.
	 * @var int
	 * @access private
	 */
	private $Index = 0;

	/**
	 * Sets Location instance with given name and returns true or throws an exception if given location is empty.
	 * @access public
	 * @param string $Name
	 * @param Location $Location
	 * @uses \Framework\Location\LocationManager::$Location
	 * @return bool
	 * @throws \Framework\Location\Exceptions\LocationManagerException if given location is empty.
	 * @see \Framework\Location\Location::GetLocation()
	 */
	public function SetLocation($Name, Location $Location) {
		if ((isset($Name)) && (is_string($Name)) && (!empty($Name)) && (isset($Location)) && (!empty($Location))) {
			$this->Location[$Name][] = $Location;
			return true;
		}
		else {
			throw new Exceptions\LocationManagerException('Given location is empty.');
		}
	}

	/**
	 * Gets Location instance with given name or throws an exception if name is empty or such location instance was not set before.
	 * @access public
	 * @param string $Name
	 * @uses \Framework\Location\LocationManager::$Location
	 * @uses \Framework\Location\LocationManager::$Index
	 * @return string
	 * @throws \Framework\Location\Exceptions\LocationManagerException if name is empty or such location instance was not set before.
	 * @see \Framework\Location\Location::SetLocation()
	 */
	public function GetLocation($Name) {
		if ((isset($Name)) && (is_string($Name)) && (!empty($Name))) {
			if ((isset($this->Location[$Name])) && (!empty($this->Location[$Name]))) {
				if (!isset($this->Location[$Name][$this->Index])) $this->Index = 0;
				return $this->Location[$Name][$this->Index++];
			} else {
				throw new Exceptions\LocationManagerException('Given location is empty.');
			}
		}
		else {
			throw new Exceptions\LocationManagerException('Location is empty.');
		}
	}
}
?>