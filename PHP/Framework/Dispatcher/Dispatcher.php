<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Dispatcher;

use \Framework\Root\RootObject;
use \Framework\Router;
use \Framework\Bootstrap;

class Dispatcher extends RootObject {
	public function __construct() {
		$this->BootstrapRouter();
	}

	private function BootstrapRouter() {
		$BootstrapRouter = new Router\BootstrapRouter();
		if ($BootstrapRouter->IsWWW()) {
			new Bootstrap\WWWBootstrap();
		}
		elseif ($BootstrapRouter->IsCLI()) {
			new Bootstrap\CLIBootstrap();
		}
		elseif ($BootstrapRouter->IsWS()) {
			new Bootstrap\WSBootstrap();
		}
	}
}
?>