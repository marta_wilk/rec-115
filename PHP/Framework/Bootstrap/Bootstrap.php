<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Bootstrap;

use \Framework\Root\RootObject;

abstract class Bootstrap extends RootObject {
	public function __construct() {

	}
}
?>