<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Bootstrap;

use \Framework\ClassLoad\ClassLoader as ClassLoader;
use \Framework\ClassLoad\ClassLoaderManager;
use \Framework\Exception\ExceptionHandlerManager;
use \Framework\Exception\Handler as ExceptionHandler;
use \Framework\Error\ErrorHandlerManager;
use \Framework\Error\Handler as ErrorHandler;
use \Framework\Database\DatabaseConnectionManager;
use \Framework\Database\DatabaseConnection;
use \Framework\Database\DatabaseConnectionSettings;
use \Framework\Template\Template;
use \Framework\Router\WWWRouter;

class WWWBootstrap extends Bootstrap {
	public $ClassLoaderManager;
	public $ExceptionHandlerManager;
	public $ErrorHandlerManager;
	public $DatabaseConnectionManager;
	public $Template;
	public $Router;

	public function __construct() {
		$this->ClassLoad();
		$this->Exception();
		$this->Error();
		$this->Database();
		$this->Template();
		$this->Router();
	}

	private function ClassLoad() {
		$Filesystem = new ClassLoader\FilesystemClassLoader(
			array(
				'BasePath' => dirname(EF_DIR),
				'Path' => array(
					'Framework' . DS,
					'Application' . DS
				),
				'Recursive' => true,
				'Extension' => array(
					'.php',
					'.inc.php',
					'.class.php'
				)
			)
		);

		$Namespace = new ClassLoader\NamespaceClassLoader(
			array(
				'BasePath' => dirname(EF_DIR),
				'Extension' => array(
					'.php',
					'.inc.php',
					'.class.php'
				)
			)
		);

		$Multi = new ClassLoader\MultiClassLoader();
		$Multi->SetClassLoader('Namespace', $Namespace);
		$Multi->SetClassLoader('Filesystem', $Filesystem);

		$this->ClassLoaderManager = new ClassLoaderManager();
		$this->ClassLoaderManager->SetClassLoader('Multi', $Multi);
		$this->ClassLoaderManager->GetClassLoader('Multi')->RegisterClassLoader();
	}

	private function Exception() {
		$this->ExceptionHandlerManager = new ExceptionHandlerManager();
		$this->ExceptionHandlerManager->SetExceptionHandler('Html', new ExceptionHandler\HtmlExceptionHandler());
	}

	private function Error() {
		$this->ErrorHandlerManager = new ErrorHandlerManager();
		$this->ErrorHandlerManager->SetErrorHandler('Html', new ErrorHandler\HtmlErrorHandler());
	}

	private function Database() {
		$this->DatabaseConnectionManager = new DatabaseConnectionManager();
		$this->DatabaseConnectionManager->SetDatabaseConnection(
			'Root',
			new DatabaseConnection(
				new DatabaseConnectionSettings(
					'localhost',
					3306,
					'entire',
					'entire',
					'entire'
				),
				true
			)
		);
	}

	private function Template() {
		$this->Template = new Template();
	}

	private function Router() {
		$Routes = array(
			'Default' => array('Controller' => '\Application\Cms\System\Error404'),
		);

		$this->Router = new WWWRouter($this, $Routes);
		$this->Router->Route();
	}
}
?>