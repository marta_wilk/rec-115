<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Router;

use \Framework\Root\RootObject;
use \Framework\Bootstrap;

class BootstrapRouter extends RootObject {
	const MODE_CLI = 'CLI';
	const MODE_WWW = 'WWW';
	const MODE_WS = 'WS';
	private $BootstrapType;

	public function __construct() {
		if (php_sapi_name() === 'cli') {
			$this->BootstrapType = self::MODE_CLI;
		}
		else {
			if ((isset($_REQUEST['Bootstrap'])) && (!empty($_REQUEST['Bootstrap']))) {
				switch ($_REQUEST['Bootstrap']) {
					case self::MODE_WWW :	$this->BootstrapType = self::MODE_WWW; break;
					case self::MODE_CLI :	$this->BootstrapType = self::MODE_CLI; break;
					case self::MODE_WS :	$this->BootstrapType = self::MODE_WS; break;
					default :				$this->BootstrapType = self::MODE_WWW; break;
				}
			}
			else {
				$this->BootstrapType = self::MODE_WWW;
			}
		}
	}

	public function GetBootstrapType() {
		return $this->BootstrapType;
	}

	public function IsWWW() {
		return ($this->GetBootstrapType() == self::MODE_WWW);
	}

	public function IsCLI() {
		return ($this->GetBootstrapType() == self::MODE_CLI);
	}

	public function IsWS() {
		return ($this->GetBootstrapType() == self::MODE_WS);
	}

	public function GetBootstrap() {
		if ($this->IsWWW()) return new Bootstrap\WWWBootstrap();
		if ($this->IsCLI()) return new Bootstrap\CLIBootstrap();
		if ($this->IsWS()) return new Bootstrap\WSBootstrap();
	}
}
?>