<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Router\Interfaces;

use \Framework\Root\RootInterface;

interface RouterInterface extends RootInterface {
	/**
	 * Route()
	 *
	 * @return
	 */
	public function Route();
}
?>