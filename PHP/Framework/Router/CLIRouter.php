<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Router;

use \Framework\Router\Interfaces as Interfaces;
use \Framework\Router\Exceptions as Exceptions;
use \Framework\Bootstrap as Bootstrap;

class CLIRouter extends Router implements Interfaces\RouterInterface {
	public $Bootstrap;
	public $Routes;

	public function __construct(Bootstrap\CLIBootstrap $Bootstrap, $Routes = null) {
		$this->Bootstrap = $Bootstrap;
		if ((isset($Routes)) && (is_array($Routes)) && (!empty($Routes))) {
			$this->Routes = $Routes;
		}
		else {
			throw new Exceptions\CLIRouterException('You need to pass at least default route.');
		}
	}

	public function Route() {
		if ((isset($_SERVER['argv'][1])) && (!empty($_SERVER['argv'][1]))) {
			if ((isset($this->Routes)) && (is_array($this->Routes)) && (!empty($this->Routes))) {
				$Route = ((isset($this->Routes[$_SERVER['argv'][1]])) && (!empty($this->Routes[$_SERVER['argv'][1]]))) ? $this->Routes[$_SERVER['argv'][1]] : $this->Routes['Default'];
				new $Route['Controller']($this->Bootstrap, $Route);
			}
			else {
				throw new Exceptions\CLIRouterException('URL do not contain controller information.');
			}
		}
		else {
			throw new Exceptions\CLIRouterException('URL do not contain controller information.');
		}
	}
}
?>