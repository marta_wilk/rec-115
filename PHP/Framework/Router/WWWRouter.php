<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Router;

use \Framework\Router\Interfaces as Interfaces;
use \Framework\Router\Exceptions as Exceptions;
use \Framework\Bootstrap as Bootstrap;

class WWWRouter extends Router implements Interfaces\RouterInterface {
	public $Bootstrap;
	public $Routes;

	public function __construct(Bootstrap\WWWBootstrap $Bootstrap, $Routes = null) {
		$this->Bootstrap = $Bootstrap;
		if ((isset($Routes)) && (is_array($Routes)) && (!empty($Routes))) {
			$this->Routes = $Routes;
		}
		else {
			throw new Exceptions\WWWRouterException('You need to pass at least default route.');
		}
	}

	public function Route() {
		if ((isset($_REQUEST['Controller'])) && (!empty($_REQUEST['Controller']))) {
			$Route = $_REQUEST['Controller'];
			new $Route($this->Bootstrap, $_REQUEST);
		}
		else if ((isset($this->Routes)) && (is_array($this->Routes)) && (!empty($this->Routes))) {
			$Matcher = ((isset($_SERVER['REDIRECT_URL'])) && (!empty($_SERVER['REDIRECT_URL']))) ? $_SERVER['REDIRECT_URL'] : '/';
			foreach($this->Routes as $Url => $Parameters) {
				foreach ($Parameters as $ParamKey => $ParamValue) {
					$Url = str_replace('[' . $ParamKey . ']', $ParamValue, $Url);
				}
				$Matches = array();
				preg_match_all($Url, $Matcher, $Matches, PREG_SET_ORDER);
				if (count($Matches) > 0) {
					foreach($Matches[0] as $Key => $Value) {
						if (is_numeric($Key)) unset($Matches[0][$Key]);
					}
					$Route = array_merge($Parameters, $Matches[0], $_REQUEST);
					new $Route['Controller']($this->Bootstrap, $Route);
					break;
				}
			}

			if (!isset($Route)) {
				if (isset($this->Routes['/Default/'])) {
					$Route = $this->Routes['/Default/'];
					new $Route['Controller']($this->Bootstrap, $Route);
				}
				else {
					throw new Exceptions\WWWRouterException('URL do not contain controller information.');
				}
			}
		}
		else if ((isset($this->Routes)) && (is_array($this->Routes)) && (!empty($this->Routes))) {
			if (isset($this->Routes['/Default/'])) {
				$Route = $this->Routes['/Default/'];
				new $Route['Controller']($this->Bootstrap, $Route);
			}
			else {
				throw new Exceptions\WWWRouterException('URL do not contain controller information.');
			}
		}
		else {
			throw new Exceptions\WWWRouterException('URL do not contain controller information.');
		}
	}
}
?>