<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Router
 */
namespace Framework\Router;

use \Framework\Root\RootObject;
use \Framework\Bootstrap\Bootstrap;

/**
 * Base class for more specific routers.
 * @access public
 */
abstract class Router extends RootObject implements Interfaces\RouterInterface {

	/**
	 * Holds instance of bootstrap class.
	 * @var Bootstrap
	 */
	public $Bootstrap;

	/**
	 * Router::__construct()
	 *
	 * @param Bootstrap $Bootstrap
	 * @uses \Framework\Router\Router::$Bootstrap
	 * @return void
	 */
	public function __construct(Bootstrap $Bootstrap) {
		$this->Bootstrap = $Bootstrap;
	}
}
?>