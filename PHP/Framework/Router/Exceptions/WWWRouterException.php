<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Router\Exceptions;

/**
 * WWW related router exception
 * @package Framework\Router\Exceptions
 * @access public
 */
class WWWRouterException extends RouterException {

}
?>