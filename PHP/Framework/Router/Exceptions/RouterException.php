<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Router\Exceptions;

use \Framework\Root\RootException;

/**
 * Common ancestor for all router related exceptions
 * @package Framework\Router\Exceptions
 * @access public
 */
class RouterException extends RootException {

}
?>