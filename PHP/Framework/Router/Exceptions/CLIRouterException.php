<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Router\Exceptions;

/**
 * CLI related router exception
 * @package Framework\Router\Exceptions
 * @access public
 */
class CLIRouterException extends RouterException {

}
?>