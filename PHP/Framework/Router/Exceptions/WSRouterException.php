<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Router\Exceptions;

/**
 * WS related router exception
 * @package Framework\Router\Exceptions
 * @access public
 */
class WSRouterException extends RouterException {

}
?>