<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Root
 */
namespace Framework\Root;

use \Exception;

/**
 * Just root exception
 * @access public
 */
abstract class RootException extends Exception {

}
?>