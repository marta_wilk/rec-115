<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Root\MVC
 */
namespace Framework\Root\MVC;

use \Framework\Root\RootObject;

/**
 * Just root controller
 * @access public
 */
abstract class Controller extends RootObject {

}
?>