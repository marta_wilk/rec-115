<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Root\MVC
 */
namespace Framework\Root\MVC;

use \Framework\Root\RootObject;

/**
 * Just root model
 * @access public
 */
abstract class Model extends RootObject {
	protected $TableName = 'table';
	protected $PrimaryKey = 'field';
	protected $SingleResultFunction = 'FetchObject';
	protected $MultipleResultsFunction = 'FetchObjectAll';
	protected $DatabaseConnection;

	public function __construct($DatabaseConnection) {
		$this->DatabaseConnection = $DatabaseConnection;
	}

	public function GetOne($PrimaryValue) {
		$PrimaryValue = mysql_real_escape_string($PrimaryValue);
		$SQL = "SELECT * FROM `" . $this->TableName . "` WHERE `" . $this->PrimaryKey . "` = '" . $PrimaryValue . "' LIMIT 1";
		$Query = $this->DatabaseConnection->Query($SQL);
		$Result = $this->DatabaseConnection->{$this->SingleResultFunction}($Query);
		return $Result;
	}

	public function GetAll() {
		$SQL = "SELECT * FROM `" . $this->TableName . "`";
		$Query = $this->DatabaseConnection->Query($SQL);
		$Result = $this->DatabaseConnection->{$this->MultipleResultsFunction}($Query);
		return $Result;
	}
}
?>