<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Root
 */
namespace Framework\Root;

/**
 * Just root interface
 * @access public
 */
interface RootInterface {

}
?>