<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\ClassLoad\Exceptions;

use \Framework\Root\RootException;

/**
 * ClassLoaderException
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class ClassLoaderException extends RootException {

}
?>