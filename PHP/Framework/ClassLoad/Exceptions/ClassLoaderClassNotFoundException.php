<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\ClassLoad\Exceptions;

/**
 * ClassLoaderClassNotFoundException
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class ClassLoaderClassNotFoundException extends ClassLoaderException {

}
?>