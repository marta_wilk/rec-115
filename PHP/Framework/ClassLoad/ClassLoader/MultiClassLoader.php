<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\ClassLoad\ClassLoader;

use \Framework\Root\RootObject;
use \Framework\ClassLoad\Interfaces as Interfaces;
use \Framework\ClassLoad\Exceptions as Exceptions;

class MultiClassLoader extends ClassLoader implements Interfaces\ClassLoaderInterface {
	private $ClassLoaders = array();

	public function __construct($Params = null) {

	}

	public function SetClassLoader($Name, Interfaces\ClassLoaderInterface $ClassLoader) {
		if ((isset($Name)) && (!empty($Name))) {
			if ((isset($ClassLoader)) && (!empty($ClassLoader))) {
				$this->ClassLoaders[$Name] = $ClassLoader;
			}
			else {
				throw new Exceptions\ClassLoaderManagerException('Given ClassLoader is empty: ' . $Name);
			}
		}
		else {
			throw new Exceptions\ClassLoaderManagerException('Given ClassLoader is empty');
		}
	}

	public function GetClassLoader($Name) {
		if ((isset($Name)) && (!empty($Name))) {
			if ((isset($this->ClassLoaders[$Name])) && (!empty($this->ClassLoaders[$Name]))) {
				return $this->ClassLoaders[$Name];
			}
			else {
				throw new Exceptions\ClassLoaderManagerException('Given ClassLoader was not found: ' . $Name);
			}
		}
		else {
			throw new Exceptions\ClassLoaderManagerException('Given ClassLoader is empty');
		}
	}

	public function UnsetClassLoader($Name) {
		if ((isset($Name)) && (!empty($Name))) {
			if ((isset($this->ClassLoaders[$Name])) && (!empty($this->ClassLoaders[$Name]))) {
				unset($this->ClassLoaders[$Name]);
			}
			else {
				throw new Exceptions\ClassLoaderManagerException('Given ClassLoader was not found: ' . $Name);
			}
		}
		else {
			throw new Exceptions\ClassLoaderManagerException('Given ClassLoader is empty');
		}
	}

	public function GetClassLoaders() {
		if ((isset($this->ClassLoaders)) && (!empty($this->ClassLoaders))) {
			return $this->ClassLoaders;
		}
		else {
			throw new Exceptions\ClassLoaderManagerException('No ClassLoaders');
		}
	}

	public function UnsetClassLoaders() {
		if ((isset($this->ClassLoaders[$Name])) && (!empty($this->ClassLoaders[$Name]))) {
			foreach ($this->ClassLoaders as $Name => $ClassLoader) {
				unset($this->ClassLoaders[$Name]);
			}
			$this->ClassLoaders = array();
		}
		else {
			throw new Exceptions\ClassLoaderManagerException('Given ClassLoader is empty');
		}
	}

	public function ClassLoad($ClassName) {
		$ClassLoaderException = null;
		foreach ($this->GetClassLoaders() as $ClassLoader) {
			try {
				$ClassLoader->ClassLoad($ClassName);
				return;
			}
			catch (Exceptions\ClassLoaderClassNotFoundException $Exception) {
				$ClassLoaderException = $Exception;
			}
		}

		if ((isset($ClassLoaderException)) && (!empty($ClassLoaderException)) && ($ClassLoaderException instanceof Exceptions\ClassLoaderClassNotFoundException)) {
			throw new Exceptions\ClassLoaderClassNotFoundException('Given class could not be found: ' . $ClassName);
		}
	}
}
?>