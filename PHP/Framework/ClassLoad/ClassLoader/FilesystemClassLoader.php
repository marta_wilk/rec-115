<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\ClassLoad\ClassLoader;

use \Framework\Root\RootObject;
use \Framework\ClassLoad\Interfaces as Interfaces;
use \Framework\ClassLoad\Exceptions as Exceptions;

class FilesystemClassLoader extends ClassLoader implements Interfaces\ClassLoaderInterface {
	private $BasePath;
	private $Paths = array();
	private $Extensions = array();

	public function __construct($Params = null) {
		if ((isset($Params)) && (!empty($Params)) && (is_array($Params))) {
			if ((isset($Params['BasePath'])) && (!empty($Params['BasePath']))) {
				$this->SetBasePath($Params['BasePath']);
			}
			if ((isset($Params['Path'])) && (!empty($Params['Path']))) {
				$this->SetPath($Params['Path'], ((isset($Params['Recursive'])) && ($Params['Recursive'] == true)) ? true : false);
			}
			if ((isset($Params['Extension'])) && (!empty($Params['Extension']))) {
				$this->SetExtension($Params['Extension']);
			}
		}
	}

	public function SetBasePath($BasePath) {
		if ((isset($BasePath)) && (!empty($BasePath))) {
			$this->BasePath = rtrim($BasePath, DS) . DS;
			chdir($this->BasePath);
		}
		else {
			throw new Exceptions\ClassLoaderBasePathException('Given base path is empty');
		}
	}

	public function GetBasePath() {
		if ((isset($this->BasePath)) && (!empty($this->BasePath))) {
			return $this->BasePath;
		}
		else {
			throw new Exceptions\ClassLoaderBasePathException('Base path is empty');
		}
	}

	public function SetPath($Path, $Recursion = true) {
		if ((isset($Path)) && (!empty($Path))) {
			if (is_array($Path)) {
				foreach ($Path as $_Path) {
					$this->SetPath($_Path, $Recursion);
				}
			}
			else {
				$Path = rtrim($Path, DS) . DS;
				if ((file_exists($Path)) && (is_dir($Path)) && (is_readable($Path))) {
					if (!in_array($Path, $this->Paths)) $this->Paths[] = $Path;
					if ((isset($Recursion)) && ($Recursion == true)) {
						$Elements = array_diff(scandir($Path), array('.', '..', '.svn', '.git', 'nbproject'));
						foreach ($Elements as $Element) {
							if ((is_dir($Path . $Element)) && (!in_array($Path . $Element . DS, $this->Paths))) {
								$this->SetPath($Path . $Element . DS, true);
							}
						}
					}
				}
			}
		}
		else {
			throw new Exceptions\ClassLoaderPathException('Given path is empty');
		}
	}

	public function GetPaths() {
		if ((isset($this->Paths)) && (!empty($this->Paths))) {
			return $this->Paths;
		}
		else {
			throw new Exceptions\ClassLoaderPathException('Paths are empty');
		}
	}

	public function SetExtension($Extension) {
		if ((isset($Extension)) && (!empty($Extension))) {
			if (is_array($Extension)) {
				foreach ($Extension as $_Extension) {
					$this->SetExtension($_Extension);
				}
			}
			else {
				$Extension = (substr($Extension, 0, 1) != '.') ? $Extension = '.' . $Extension : $Extension;
				if (!in_array($Extension, $this->Extensions)) {
					$this->Extensions[] = $Extension;
				}
			}
		}
		else {
			throw new Exceptions\ClassLoaderExtensionException('Given extension is empty');
		}
	}

	public function GetExtensions() {
		if ((isset($this->Extensions)) && (!empty($this->Extensions))) {
			return $this->Extensions;
		}
		else {
			throw new Exceptions\ClassLoaderExtensionException('Extensions are empty');
		}
	}

	public function ClassLoad($ClassName) {
		foreach ($this->GetPaths() as $Path) {
			foreach ($this->GetExtensions() as $Extension) {
				if (file_exists($this->GetBasePath() . $Path . $ClassName . $Extension)) {
					require_once $this->GetBasePath() . $Path . $ClassName . $Extension;
					return true;
				}
			}
		}

		throw new Exceptions\ClassLoaderClassNotFoundException('Given class could not be found: ' . $ClassName);
	}
}
?>