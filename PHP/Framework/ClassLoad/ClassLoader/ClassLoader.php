<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\ClassLoad\ClassLoader;

use \Framework\Root\RootObject;
use \Framework\ClassLoad\Interfaces as Interfaces;
use \Framework\ClassLoad\Exceptions as Exceptions;

abstract class ClassLoader extends RootObject implements Interfaces\ClassLoaderInterface {
	public function __construct($Params = null) {

	}

	public function RegisterClassLoader() {
		spl_autoload_register(array($this, 'ClassLoad'));
	}

	public function UnregisterClassLoader() {
		spl_autoload_unregister(array($this, 'ClassLoad'));
	}
}
?>