<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\ClassLoad\ClassLoader;

use \Framework\Root\RootObject;
use \Framework\ClassLoad\Interfaces as Interfaces;
use \Framework\ClassLoad\Exceptions as Exceptions;

class NamespaceClassLoader extends ClassLoader implements Interfaces\ClassLoaderInterface {
	private $BasePath;
	private $Extensions = array();

	public function __construct($Params = null) {
		if ((isset($Params)) && (!empty($Params)) && (is_array($Params))) {
			if ((isset($Params['BasePath'])) && (!empty($Params['BasePath']))) {
				$this->SetBasePath($Params['BasePath']);
			}
			if ((isset($Params['Extension'])) && (!empty($Params['Extension']))) {
				$this->SetExtension($Params['Extension']);
			}
		}
	}

	public function SetBasePath($BasePath) {
		if ((isset($BasePath)) && (!empty($BasePath))) {
			$this->BasePath = rtrim($BasePath, DS) . DS;
			chdir($this->BasePath);
		}
		else {
			throw new Exceptions\ClassLoaderBasePathException('Given base path is empty');
		}
	}

	public function GetBasePath() {
		if ((isset($this->BasePath)) && (!empty($this->BasePath))) {
			return $this->BasePath;
		}
		else {
			throw new Exceptions\ClassLoaderBasePathException('Base path is empty');
		}
	}

	public function SetExtension($Extension) {
		if ((isset($Extension)) && (!empty($Extension))) {
			if (is_array($Extension)) {
				foreach ($Extension as $_Extension) {
					$this->SetExtension($_Extension);
				}
			}
			else {
				$Extension = (substr($Extension, 0, 1) != '.') ? $Extension = '.' . $Extension : $Extension;
				if (!in_array($Extension, $this->Extensions)) {
					$this->Extensions[] = $Extension;
				}
			}
		}
		else {
			throw new Exceptions\ClassLoaderExtensionException('Given extension is empty');
		}
	}

	public function GetExtensions() {
		if ((isset($this->Extensions)) && (!empty($this->Extensions))) {
			return $this->Extensions;
		}
		else {
			throw new Exceptions\ClassLoaderExtensionException('Extensions are empty');
		}
	}

	public function ClassLoad($ClassName) {
		$Path = str_replace('\\', DS, $ClassName);
		foreach ($this->GetExtensions() as $Extension) {
			if (file_exists($this->GetBasePath() . $Path . $Extension)) {
				require_once $this->GetBasePath() . $Path . $Extension;
				return;
			}
		}

		throw new Exceptions\ClassLoaderClassNotFoundException('Given class could not be found: ' . $ClassName);
	}
}
?>