<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\ClassLoad;

use \Framework\Root\RootObject;
use \Framework\ClassLoad\Interfaces as Interfaces;
use \Framework\ClassLoad\Exceptions as Exceptions;

/**
 * ClassLoaderManager
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class ClassLoaderManager extends RootObject {
	private $ClassLoaders;

	/**
	 * ClassLoaderManager::__construct()
	 *
	 * @return
	 */
	public function __construct() {

	}

	/**
	 * ClassLoaderManager::SetClassLoader()
	 *
	 * @param mixed $Name
	 * @param mixed $ClassLoader
	 * @return
	 */
	public function SetClassLoader($Name, Interfaces\ClassLoaderInterface $ClassLoader) {
		if ((isset($Name)) && (!empty($Name))) {
			if ((isset($ClassLoader)) && (!empty($ClassLoader))) {
				$this->ClassLoaders[$Name] = $ClassLoader;
			}
			else {
				throw new Exceptions\ClassLoaderManagerException('Given ClassLoader is empty: ' . $Name);
			}
		}
		else {
			throw new Exceptions\ClassLoaderManagerException('Given ClassLoader is empty');
		}
	}

	/**
	 * ClassLoaderManager::GetClassLoader()
	 *
	 * @param mixed $Name
	 * @return
	 */
	public function GetClassLoader($Name) {
		if ((isset($Name)) && (!empty($Name))) {
			if ((isset($this->ClassLoaders[$Name])) && (!empty($this->ClassLoaders[$Name]))) {
				return $this->ClassLoaders[$Name];
			}
			else {
				throw new Exceptions\ClassLoaderManagerException('Given ClassLoader was not found: ' . $Name);
			}
		}
		else {
			throw new Exceptions\ClassLoaderManagerException('Given ClassLoader is empty');
		}
	}

	/**
	 * ClassLoaderManager::UnsetClassLoader()
	 *
	 * @param mixed $Name
	 * @return
	 */
	public function UnsetClassLoader($Name) {
		if ((isset($Name)) && (!empty($Name))) {
			if ((isset($this->ClassLoaders[$Name])) && (!empty($this->ClassLoaders[$Name]))) {
				unset($this->ClassLoaders[$Name]);
			}
			else {
				throw new Exceptions\ClassLoaderManagerException('Given ClassLoader was not found: ' . $Name);
			}
		}
		else {
			throw new Exceptions\ClassLoaderManagerException('Given ClassLoader is empty');
		}
	}

	/**
	 * ClassLoaderManager::GetClassLoaders()
	 *
	 * @return
	 */
	public function GetClassLoaders() {
		if ((isset($this->ClassLoaders)) && (!empty($this->ClassLoaders))) {
			return $this->ClassLoaders;
		}
		else {
			throw new Exceptions\ClassLoaderManagerException('No ClassLoaders');
		}
	}

	/**
	 * ClassLoaderManager::UnsetClassLoaders()
	 *
	 * @return
	 */
	public function UnsetClassLoaders() {
		if ((isset($this->ClassLoaders[$Name])) && (!empty($this->ClassLoaders[$Name]))) {
			foreach ($this->ClassLoaders as $Name => $ClassLoader) {
				unset($this->ClassLoaders[$Name]);
			}
			$this->ClassLoaders = array();
		}
		else {
			throw new Exceptions\ClassLoaderManagerException('Given ClassLoader is empty');
		}
	}
}
?>