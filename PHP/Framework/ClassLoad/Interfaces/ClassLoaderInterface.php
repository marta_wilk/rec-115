<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\ClassLoad\Interfaces;

use \Framework\Root\RootInterface;

interface ClassLoaderInterface extends RootInterface {
	/**
	 * __construct()
	 *
	 * @param mixed $Params
	 * @return
	 */
	public function __construct($Params = null);
	/**
	 * ClassLoad()
	 *
	 * @param mixed $ClassName
	 * @return
	 */
	public function ClassLoad($ClassName);
	/**
	 * RegisterClassLoader()
	 *
	 * @return
	 */
	public function RegisterClassLoader();
	/**
	 * UnregisterClassLoader()
	 *
	 * @return
	 */
	public function UnregisterClassLoader();
}
?>