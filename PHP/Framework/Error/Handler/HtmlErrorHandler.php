<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Error\Handler;

use \Framework\Root\RootObject;
use \Framework\Error\Interfaces as Interfaces;

/**
 * HtmlErrorHandler
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class HtmlErrorHandler extends RootObject implements Interfaces\ErrorHandlerInterface {
	/**
	 * HtmlErrorHandler::__construct()
	 *
	 * @return
	 */
	public function __construct() {

	}

	/**
	 * HtmlErrorHandler::HandleError()
	 *
	 * @param mixed $Number
	 * @param mixed $String
	 * @param mixed $File
	 * @param mixed $Line
	 * @return
	 */
	public function HandleError($Number, $String, $File, $Line) {
		if (ini_get('error_reporting') >= $Number) {
			echo '<h1>ErrorException</h1><h2>' . $String . '</h2><h3>' . $File . ' on line ' . $Line . '</h3>';
		}
	}
}
?>