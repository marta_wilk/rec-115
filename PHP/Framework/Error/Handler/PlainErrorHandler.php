<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Error\Handler;

use \Framework\Root\RootObject;
use \Framework\Error\Interfaces as Interfaces;

/**
 * PlainErrorHandler
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class PlainErrorHandler extends RootObject implements Interfaces\ErrorHandlerInterface {
	/**
	 * PlainErrorHandler::__construct()
	 *
	 * @return
	 */
	public function __construct() {

	}

	/**
	 * PlainErrorHandler::HandleError()
	 *
	 * @param mixed $Number
	 * @param mixed $String
	 * @param mixed $File
	 * @param mixed $Line
	 * @return
	 */
	public function HandleError($Number, $String, $File, $Line) {
		if (ini_get('error_reporting') >= $Number) {
			header('Content-Type: text/plain');
			echo 'ErrorException', "\n", str_repeat('-', strlen('ErrorException')), "\n", $String, "\n\n", $File, " on line ", $Line, "\n";
		}
	}
}
?>