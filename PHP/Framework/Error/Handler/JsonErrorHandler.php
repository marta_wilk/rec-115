<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Error\Handler;

use \Framework\Root\RootObject;
use \Framework\Error\Interfaces as Interfaces;

/**
 * JsonErrorHandler
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class JsonErrorHandler extends RootObject implements Interfaces\ErrorHandlerInterface {
	/**
	 * JsonErrorHandler::__construct()
	 *
	 * @return
	 */
	public function __construct() {

	}

	/**
	 * JsonErrorHandler::HandleError()
	 *
	 * @param mixed $Number
	 * @param mixed $String
	 * @param mixed $File
	 * @param mixed $Line
	 * @return
	 */
	public function HandleError($Number, $String, $File, $Line) {
		if (ini_get('error_reporting') >= $Number) {
			echo json_encode(array('Class' => 'ErrorException', 'Message' => $String, 'File' => $File, 'Line' => $Line));
		}
	}
}
?>