<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Error\Handler;

use \Framework\Root\RootObject;
use \Framework\Error\Interfaces as Interfaces;

/**
 * FileErrorHandler
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class FileErrorHandler extends RootObject implements Interfaces\ErrorHandlerInterface {
	protected $FilePath;
	protected $Format;

	/**
	 * FileErrorHandler::__construct()
	 *
	 * @param mixed $FilePath
	 * @param mixed $Format
	 * @return
	 */
	public function __construct($FilePath, $Format) {
		if ((isset($FilePath)) && (!empty($FilePath))) {
			$this->FilePath = $FilePath;
		}
		//---
		if ((isset($Format)) && (!empty($Format))) {
			$this->Format = ($Format == 'Long') ? 'Long' : 'Short';
		}
	}

	/**
	 * FileErrorHandler::HandleError()
	 *
	 * @param mixed $Number
	 * @param mixed $String
	 * @param mixed $File
	 * @param mixed $Line
	 * @return
	 */
	public function HandleError($Number, $String, $File, $Line) {
		if (ini_get('error_reporting') >= $Number) {
			$Handler = fopen($this->FilePath, 'a');
			if (flock($Handler, LOCK_EX)) {
				switch ($this->Format) {
					case 'Long':	fwrite($Handler, 'ErrorException on ' . date('Y-m-d H:i:s', time()) . "\n" . str_repeat('-', strlen('ErrorException on ' . date('Y-m-d H:i:s', time()))) . "\n" . $String . "\n\n" . $File . " on line " . $Line . "\n" . str_repeat('-', 80) . "\n\n");
									break;
					case 'Short':	fwrite($Handler, date('Y-m-d H:i:s', time()) . ' | ErrorException | '. $String . ' | ' . $File . ' on line ' . $Line . "\n");
									break;
				}
				fflush($Handler);
				flock($Handler, LOCK_UN);
				fclose($Handler);
			}
		}
	}
}
?>