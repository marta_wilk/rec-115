<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Error\Handler;

use \Framework\Root\RootObject;
use \Framework\Error\Interfaces as Interfaces;

/**
 * JavaScriptErrorHandler
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class JavaScriptErrorHandler extends RootObject implements Interfaces\ErrorHandlerInterface {
	/**
	 * JavaScriptErrorHandler::__construct()
	 *
	 * @return
	 */
	public function __construct() {

	}

	/**
	 * JavaScriptErrorHandler::HandleError()
	 *
	 * @param mixed $Number
	 * @param mixed $String
	 * @param mixed $File
	 * @param mixed $Line
	 * @return
	 */
	public function HandleError($Number, $String, $File, $Line) {
		if (ini_get('error_reporting') >= $Number) {
			echo '<script type="text/javascript">alert("Class: ErrorException\n\nMessage: ' . addslashes($String) . '\n\nFile: ' . addslashes($File) . '\nLine: ' . $Line . '");</script>';
		}
	}
}
?>