<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Error;

use \Framework\Root\RootObject;
use \Framework\Error\Interfaces as Interfaces;
use \Framework\Error\Exceptions as Exceptions;

/**
 * ErrorHandlerManager
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class ErrorHandlerManager extends RootObject {
	private $ErrorHandlers;

	/**
	 * ErrorHandlerManager::__construct()
	 *
	 * @return
	 */
	public function __construct() {
		if (func_num_args() == 1) {
			$this->SetErrorHandler(func_get_arg(0));
		}
	}

	/**
	 * ErrorHandlerManager::SetErrorHandler()
	 *
	 * @param mixed $Name
	 * @param mixed $ErrorHandler
	 * @return
	 */
	public function SetErrorHandler($Name, Interfaces\ErrorHandlerInterface $ErrorHandler) {
		if ((isset($Name)) && (!empty($Name)) && (isset($ErrorHandler)) && (!empty($ErrorHandler))) {
			if (is_callable(array($ErrorHandler, 'HandleError'))) {
				$this->ErrorHandlers[$Name] = $ErrorHandler;
				set_error_handler(array($ErrorHandler, 'HandleError'));
			}
			else {
				throw new Exceptions\ErrorHandlerManagerException('Given error handler seems to be invalid: ' . $Name);
			}
		}
		else {
			throw new Exceptions\ErrorHandlerManagerException('Given error handler is empty.');
		}
	}

	/**
	 * ErrorHandlerManager::GetErrorHandler()
	 *
	 * @param mixed $Name
	 * @return
	 */
	public function GetErrorHandler($Name) {
		if ((isset($Name)) && (!empty($Name))) {
			if ((isset($this->ErrorHandlers[$Name])) && (!empty($this->ErrorHandlers[$Name]))) {
				return $this->ErrorHandlers[$Name];
			}
			else {
				throw new Exceptions\ErrorHandlerManagerException('Error handler is empty.');
			}
		}
		else {
			throw new Exceptions\ErrorHandlerManagerException('Given error handler is empty.');
		}
	}

	/**
	 * ErrorHandlerManager::UnsetErrorHandler()
	 *
	 * @param mixed $Name
	 * @return
	 */
	public function UnsetErrorHandler($Name) {
		if ((isset($Name)) && (!empty($Name))) {
			if ((isset($this->ErrorHandlers[$Name])) && (!empty($this->ErrorHandlers[$Name]))) {
				restore_error_handler();
				unset($this->ErrorHandlers[$Name]);

				if ((isset($this->ErrorHandlers)) && (!empty($this->ErrorHandlers))) {
					$ErrorHandler = end($this->ErrorHandlers);
					set_error_handler(array($ErrorHandler, 'HandleError'));
				}
			}
			else {
				throw new Exceptions\ErrorHandlerManagerException('Error handler is empty.');
			}
		}
		else {
			throw new Exceptions\ErrorHandlerManagerException('Given error handler is empty.');
		}
	}
}
?>