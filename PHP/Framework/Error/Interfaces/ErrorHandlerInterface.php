<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Error\Interfaces;

use \Framework\Root\RootInterface;

interface ErrorHandlerInterface extends RootInterface {
	/**
	 * HandleError()
	 *
	 * @param mixed $Number
	 * @param mixed $String
	 * @param mixed $File
	 * @param mixed $Line
	 * @return
	 */
	public function HandleError($Number, $String, $File, $Line);
}
?>