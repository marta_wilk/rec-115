<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Error\Exceptions;

use \Framework\Root\RootException;

/**
 * ErrorHandlerException
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class ErrorHandlerException extends RootException {

}
?>