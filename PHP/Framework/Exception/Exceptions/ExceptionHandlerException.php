<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Exception\Exceptions;

use \Framework\Root\RootException;

/**
 * ExceptionHandlerException
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class ExceptionHandlerException extends RootException {

}
?>