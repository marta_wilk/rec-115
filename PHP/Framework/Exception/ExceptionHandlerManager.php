<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Exception;

use \Framework\Root\RootObject;
use \Framework\Exception\Interfaces as Interfaces;
use \Framework\Exception\Exceptions as Exceptions;

/**
 * ExceptionHandlerManager
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class ExceptionHandlerManager extends RootObject {
	private $ExceptionHandlers;

	/**
	 * ExceptionHandlerManager::__construct()
	 *
	 * @return
	 */
	public function __construct() {
		if (func_num_args() == 1) {
			$this->SetExceptionHandler(func_get_arg(0));
		}
	}

	/**
	 * ExceptionHandlerManager::SetExceptionHandler()
	 *
	 * @param mixed $Name
	 * @param mixed $ExceptionHandler
	 * @return
	 */
	public function SetExceptionHandler($Name, Interfaces\ExceptionHandlerInterface $ExceptionHandler) {
		if ((isset($Name)) && (!empty($Name)) && (isset($ExceptionHandler)) && (!empty($ExceptionHandler))) {
			if (is_callable(array($ExceptionHandler, 'HandleException'))) {
				$this->ExceptionHandlers[$Name] = $ExceptionHandler;
				set_exception_handler(array($ExceptionHandler, 'HandleException'));
			}
			else {
				throw new Exceptions\ExceptionHandlerManagerException('Given exception handler seems to be invalid: ' . $Name);
			}
		}
		else {
			throw new Exceptions\ExceptionHandlerManagerException('Given exception handler is empty.');
		}
	}

	/**
	 * ExceptionHandlerManager::GetExceptionHandler()
	 *
	 * @param mixed $Name
	 * @return
	 */
	public function GetExceptionHandler($Name) {
		if ((isset($Name)) && (!empty($Name))) {
			if ((isset($this->ExceptionHandlers[$Name])) && (!empty($this->ExceptionHandlers[$Name]))) {
				return $this->ExceptionHandlers[$Name];
			}
			else {
				throw new Exceptions\ExceptionHandlerManagerException('Exception handler is empty.');
			}
		}
		else {
			throw new Exceptions\ExceptionHandlerManagerException('Given exception handler is empty.');
		}
	}

	/**
	 * ExceptionHandlerManager::UnsetExceptionHandler()
	 *
	 * @param mixed $Name
	 * @return
	 */
	public function UnsetExceptionHandler($Name) {
		if ((isset($Name)) && (!empty($Name))) {
			if ((isset($this->ExceptionHandlers[$Name])) && (!empty($this->ExceptionHandlers[$Name]))) {
				restore_exception_handler();
				unset($this->ExceptionHandlers[$Name]);
				//---
				if ((isset($this->ExceptionHandlers)) && (!empty($this->ExceptionHandlers))) {
					$ExceptionHandler = end($this->ExceptionHandlers);
					set_exception_handler(array($ExceptionHandler, 'HandleException'));
				}
			}
			else {
				throw new Exceptions\ExceptionHandlerManagerException('Exception handler is empty.');
			}
		}
		else {
			throw new Exceptions\ExceptionHandlerManagerException('Given exception handler is empty.');
		}
	}
}
?>