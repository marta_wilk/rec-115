<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Exception\Handler;

use \Framework\Root\RootObject;
use \Framework\Exception\Interfaces as Interfaces;

class JavaScriptExceptionHandler extends RootObject implements Interfaces\ExceptionHandlerInterface {
	public function __construct() {

	}

	public function HandleException(\Exception $Exception) {
		echo '<script type="text/javascript">alert("Class: ' . addslashes(get_class($Exception)) . '\n\nMessage: ' . addslashes($Exception->getMessage()) . '\n\nFile: ' . addslashes($Exception->getFile()) . '\nLine: ' . $Exception->getLine() . '\n\nTrace: \n' . str_replace("\n", "\\n\" + \n\"", addslashes($Exception->getTraceAsString())) . '");</script>';
	}
}
?>