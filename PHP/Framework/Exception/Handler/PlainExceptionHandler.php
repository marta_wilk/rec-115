<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Exception\Handler;

use \Framework\Root\RootObject;
use \Framework\Exception\Interfaces as Interfaces;

class PlainExceptionHandler extends RootObject implements Interfaces\ExceptionHandlerInterface {
	public function __construct() {

	}

	public function HandleException(\Exception $Exception) {
		header('Content-Type: text/plain; charset=UTF-8');
		echo get_class($Exception), "\n", str_repeat('-', strlen(get_class($Exception))) . "\n", $Exception->getMessage() . "\n\n", $Exception->getFile() . " on line " . $Exception->getLine() . "\n\n" . $Exception->getTraceAsString();
	}
}
?>