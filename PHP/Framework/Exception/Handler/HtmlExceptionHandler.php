<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Exception\Handler;

use \Framework\Root\RootObject;
use \Framework\Exception\Interfaces as Interfaces;

class HtmlExceptionHandler extends RootObject implements Interfaces\ExceptionHandlerInterface {
	public function __construct() {

	}

	public function HandleException(\Exception $Exception) {
		echo '<h1>' . get_class($Exception) . '</h1><h2>' . $Exception->getMessage() . '</h2><h3>' . $Exception->getFile() . ' on line ' . $Exception->getLine() . '</h3><h4>' . nl2br($Exception->getTraceAsString()) . '</h4>';
	}
}
?>