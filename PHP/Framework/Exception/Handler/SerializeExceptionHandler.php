<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Exception\Handler;

use \Framework\Root\RootObject;
use \Framework\Exception\Interfaces as Interfaces;

class SerializeExceptionHandler extends RootObject implements Interfaces\ExceptionHandlerInterface {
	public function __construct() {

	}

	public function HandleException(\Exception $Exception) {
		echo serialize(array('Class' => get_class($Exception), 'Message' => $Exception->getMessage(), 'File' => $Exception->getFile(), 'Line' => $Exception->getLine(), 'TraceAsString' => $Exception->getTraceAsString()));
	}
}
?>