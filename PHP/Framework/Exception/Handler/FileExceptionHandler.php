<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Exception\Handler;

use \Framework\Root\RootObject;
use \Framework\Exception\Interfaces as Interfaces;

class FileExceptionHandler extends RootObject implements Interfaces\ExceptionHandlerInterface {
	protected $FilePath;
	protected $Format;

	public function __construct($FilePath, $Format) {
		if ((isset($FilePath)) && (!empty($FilePath))) {
			$this->FilePath = $FilePath;
		}

		if ((isset($Format)) && (!empty($Format))) {
			$this->Format = ($Format == 'Long') ? 'Long' : 'Short';
		}
	}

	public function HandleException(\Exception $Exception) {
		$Handler = fopen($this->FilePath, 'a');
		if (flock($Handler, LOCK_EX)) {
			switch ($this->Format) {
				case 'Long':	fwrite($Handler, get_class($Exception) . ' on ' . date('Y-m-d H:i:s', time()) . "\n" . str_repeat('-', strlen(get_class($Exception) . ' on ' . date('Y-m-d H:i:s', time()))) . "\n" . $Exception->getMessage() . "\n\n" .  $Exception->getFile() . ' on line ' . $Exception->getLine() . "\n\n" . $Exception->getTraceAsString() . "\n" . str_repeat('-', 80) . "\n\n");
								break;
				case 'Short':	fwrite($Handler, date('Y-m-d H:i:s', time()) . ' | ' . get_class($Exception) . ' | '. $Exception->getMessage() . ' | ' .  $Exception->getFile() . ' on line ' . $Exception->getLine() . "\n");
								break;
			}
			fflush($Handler);
			flock($Handler, LOCK_UN);
			fclose($Handler);
		}
	}
}
?>