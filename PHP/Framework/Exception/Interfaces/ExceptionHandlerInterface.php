<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Exception\Interfaces;

use \Framework\Root\RootInterface;

interface ExceptionHandlerInterface extends RootInterface {
	/**
	 * HandleException()
	 *
	 * @param mixed $Exception
	 * @return
	 */
	public function HandleException(\Exception $Exception);
}
?>