<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Database
 */
namespace Framework\Database\Exceptions;

use \Framework\Root\RootException;

/**
 * General exception related to database
 */
class DatabaseConnectionException extends RootException {

}
?>