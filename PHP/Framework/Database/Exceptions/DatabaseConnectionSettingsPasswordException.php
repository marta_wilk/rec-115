<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Database
 */
namespace Framework\Database\Exceptions;

/**
 * Exception related to password setting
 */
class DatabaseConnectionSettingsPasswordException extends DatabaseConnectionSettingsException {

}
?>