<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Database
 */
namespace Framework\Database\Exceptions;

/**
 * Exception related to fetching data from query
 */
class DatabaseConnectionFetchException extends DatabaseConnectionException {

}
?>