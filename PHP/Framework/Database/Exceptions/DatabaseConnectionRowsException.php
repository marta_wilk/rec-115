<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Database
 */
namespace Framework\Database\Exceptions;

/**
 * Exception related to number of rows returned by query
 */
class DatabaseConnectionRowsException extends DatabaseConnectionException {

}
?>