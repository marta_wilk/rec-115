<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Database
 */
namespace Framework\Database\Exceptions;

/**
 * Exception related to database name setting
 */
class DatabaseConnectionSettingsDatabaseException extends DatabaseConnectionSettingsException {

}
?>