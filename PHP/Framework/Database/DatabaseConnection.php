<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Database
 */
namespace Framework\Database;

use \Framework\Root\RootObject;
use \Framework\Database\Exceptions as Exceptions;

class DatabaseConnection extends RootObject {
	private $DatabaseConnectionSettings;
	private $DatabaseConnection;

	/**
	 * DatabaseConnection::__construct()
	 *
	 * @return
	 */
	public function __construct() {
		if (func_num_args() == 1) {
			$this->SetDatabaseConnectionSettings(func_get_arg(0));
		}
		elseif (func_num_args() == 2) {
			if (func_get_arg(1) == true) {
				$this->SetDatabaseConnectionSettings(func_get_arg(0));
				$this->Connect();
				$this->SelectDB();
				$this->SetEncoding();
			}
			else {
				$this->SetDatabaseConnectionSettings(func_get_arg(0));
			}
		}
	}

	/**
	 * DatabaseConnection::SetDatabaseConnectionSettings()
	 *
	 * @param mixed $DatabaseConnectionSettings
	 * @return
	 */
	public function SetDatabaseConnectionSettings(DatabaseConnectionSettings $DatabaseConnectionSettings) {
		if ((isset($DatabaseConnectionSettings)) && (!empty($DatabaseConnectionSettings))) {
			$this->DatabaseConnectionSettings = $DatabaseConnectionSettings;
		}
		else {
			throw new Exceptions\DatabaseConnectionSettingsException('Given database connection settings are empty.');
		}
	}

	/**
	 * DatabaseConnection::GetDatabaseConnectionSettings()
	 *
	 * @return
	 */
	public function GetDatabaseConnectionSettings() {
		if ((isset($this->DatabaseConnectionSettings)) && (!empty($this->DatabaseConnectionSettings))) {
			return $this->DatabaseConnectionSettings;
		}
		else {
			throw new Exceptions\DatabaseConnectionSettingsException('Database connection settings are empty.');
		}
	}

	/**
	 * DatabaseConnection::Connect()
	 *
	 * @return
	 */
	public function Connect() {
		$this->DatabaseConnection = @mysql_connect($this->DatabaseConnectionSettings->GetHost() . ':' . $this->DatabaseConnectionSettings->GetPort(), $this->DatabaseConnectionSettings->GetUser(), $this->DatabaseConnectionSettings->GetPassword(), true);
		if (!$this->IsConnected()) {
			throw new Exceptions\DatabaseConnectionConnectException('Database connection settings are invalid. Given settings: Host: ' . $this->DatabaseConnectionSettings->GetHost() . ' Port: ' . $this->DatabaseConnectionSettings->GetPort() . ' User: ' . $this->DatabaseConnectionSettings->GetUser() . ' Password: ' . $this->DatabaseConnectionSettings->GetPassword());
		}
	}

	/**
	 * DatabaseConnection::IsConnected()
	 *
	 * @return
	 */
	public function IsConnected() {
		return (is_resource($this->DatabaseConnection));
	}

	/**
	 * DatabaseConnection::Disconnect()
	 *
	 * @return
	 */
	public function Disconnect() {
		@mysql_close($this->DatabaseConnection);
		$this->DatabaseConnection = null;
	}

	/**
	 * DatabaseConnection::SelectDb()
	 *
	 * @return
	 */
	public function SelectDb() {
		if ($this->IsConnected()) {
			@mysql_select_db($this->DatabaseConnectionSettings->GetDatabase(), $this->DatabaseConnection);
			if (mysql_errno()) {
				throw new Exceptions\DatabaseConnectionSelectException('Database connection settings are invalid. Given settings: Database: ' . $this->DatabaseConnectionSettings->GetDatabase() . ' MySQL response: ' . mysql_errno() . ': '. mysql_error());
			}
		}
		else {
			throw new Exceptions\DatabaseConnectionConnectException('There is no database connection established.');
		}
	}

	/**
	 * DatabaseConnection::SetEncoding()
	 *
	 * @return
	 */
	public function SetEncoding() {
		if ($this->IsConnected()) {
			$this->Query("SET NAMES 'utf8'");
			$this->Query("SET CHARSET 'utf8'");
			$this->Query("SET character_set_connection = 'utf8'");
			$this->Query("SET character_set_client = 'utf8'");
		}
		else {
			throw new Exceptions\DatabaseConnectionConnectException('There is no database connection established.');
		}
	}

	/**
	 * DatabaseConnection::Query()
	 *
	 * @param mixed $SQL
	 * @return
	 */
	public function Query($SQL) {
		if ($this->IsConnected()) {
			if ((isset($SQL)) && (!empty($SQL))) {
				$Query = mysql_query($SQL, $this->DatabaseConnection);
				if (!mysql_errno()) {
					return $Query;
				}
				else {
					throw new Exceptions\DatabaseConnectionQueryException(mysql_errno() . ': ' . mysql_error());
				}
			}
			else {
				throw new Exceptions\DatabaseConnectionQueryException('Given SQL statement is empty.');
			}
		}
		else {
			throw new Exceptions\DatabaseConnectionConnectException('There is no database connection established.');
		}
	}

	/**
	 * DatabaseConnection::Rows()
	 *
	 * @param mixed $Query
	 * @return
	 */
	public function Rows($Query) {
		if ($this->IsConnected()) {
			$Rows = @mysql_num_rows($Query);
			if (!mysql_errno()) {
				return $Rows;
			}
			else {
				throw new Exceptions\DatabaseConnectionRowsException(mysql_errno() . ': ' . mysql_error());
			}
		}
		else {
			throw new Exceptions\DatabaseConnectionConnectException('There is no database connection established.');
		}
	}

	/**
	 * DatabaseConnection::AffectedRows()
	 *
	 * @return
	 */
	public function AffectedRows() {
		if ($this->IsConnected()) {
			$Rows = @mysql_affected_rows($this->DatabaseConnection);
			if (!mysql_errno()) {
				return $Rows;
			}
			else {
				throw new Exceptions\DatabaseConnectionRowsException(mysql_errno() . ': ' . mysql_error());
			}
		}
		else {
			throw new Exceptions\DatabaseConnectionConnectException('There is no database connection established.');
		}
	}

	/**
	 * DatabaseConnection::InsertId()
	 *
	 * @return
	 */
	public function InsertId() {
		if ($this->IsConnected()) {
			$ID = @mysql_insert_id($this->DatabaseConnection);
			if (!mysql_errno()) {
				return $ID;
			}
			else {
				throw new Exceptions\DatabaseConnectionException(mysql_errno() . ': ' . mysql_error());
			}
		}
		else {
			throw new Exceptions\DatabaseConnectionConnectException('There is no database connection established.');
		}
	}

	/**
	 * DatabaseConnection::FetchObject()
	 *
	 * @param mixed $Query
	 * @return
	 */
	public function FetchObject($Query) {
		if ($this->IsConnected()) {
			if ((isset($Query)) && (!empty($Query))) {
				$Result = @mysql_fetch_object($Query, 'stdClass');
				if (!mysql_errno()) {
					return $Result;
				}
				else {
					throw new Exceptions\DatabaseConnectionFetchException(mysql_errno() . ': ' . mysql_error());
				}
			}
			else {
				throw new Exceptions\DatabaseConnectionFetchException('Given SQL query is empty.');
			}
		}
		else {
			throw new Exceptions\DatabaseConnectionConnectException('There is no database connection established.');
		}
	}

	/**
	 * DatabaseConnection::FetchObjectAll()
	 *
	 * @param mixed $Query
	 * @return
	 */
	public function FetchObjectAll($Query) {
		if ($this->IsConnected()) {
			if ((isset($Query)) && (!empty($Query))) {
				try {
					$Result = new \stdClass();
					if ($this->Rows($Query) > 0) {
						$i = 0;
						while ($Res = $this->FetchObject($Query)) {
							$Result->{'item_' . $i} = $Res;
							$i++;
						}
						return $Result;
					}
				}
				catch (Exception $Exception) { // ?
					throw $Exception;
				}
			}
			else {
				throw new Exceptions\DatabaseConnectionFetchException('Given SQL query is empty.');
			}
		}
		else {
			throw new Exceptions\DatabaseConnectionConnectException('There is no database connection established.');
		}
	}

	/**
	 * DatabaseConnection::FetchAssoc()
	 *
	 * @param mixed $Query
	 * @return
	 */
	public function FetchAssoc($Query) {
		if ($this->IsConnected()) {
			if ((isset($Query)) && (!empty($Query))) {
				$Result = @mysql_fetch_array($Query, MYSQL_ASSOC);
				if (!mysql_errno()) {
					return $Result;
				}
				else {
					throw new Exceptions\DatabaseConnectionFetchException(mysql_errno() . ': ' . mysql_error());
				}
			}
			else {
				throw new Exceptions\DatabaseConnectionFetchException('Given SQL query is empty.');
			}
		}
		else {
			throw new Exceptions\DatabaseConnectionConnectException('There is no database connection established.');
		}
	}

	/**
	 * DatabaseConnection::FetchAssocAll()
	 *
	 * @param mixed $Query
	 * @return
	 */
	public function FetchAssocAll($Query) {
		if ($this->IsConnected()) {
			if ((isset($Query)) && (!empty($Query))) {
				try {
					$Result = array();
					if ($this->Rows($Query) > 0) {
						$i = 0;
						while ($Res = $this->FetchAssoc($Query)) {
							$Result['item_' . $i] = $Res;
							$i++;
						}
						return $Result;
					}
				}
				catch (Exception $Exception) { // ?
					throw $Exception;
				}
			}
			else {
				throw new Exceptions\DatabaseConnectionFetchException('Given SQL query is empty.');
			}
		}
		else {
			throw new Exceptions\DatabaseConnectionConnectException('There is no database connection established.');
		}
	}

	/**
	 * DatabaseConnection::FetchArray()
	 *
	 * @param mixed $Query
	 * @return
	 */
	public function FetchArray($Query) {
		if ($this->IsConnected()) {
			if ((isset($Query)) && (!empty($Query))) {
				$Result = @mysql_fetch_array($Query, MYSQL_NUM);
				if (!mysql_errno()) {
					return $Result;
				}
				else {
					throw new Exceptions\DatabaseConnectionFetchException(mysql_errno() . ': ' . mysql_error());
				}
			}
			else {
				throw new Exceptions\DatabaseConnectionFetchException('Given SQL query is empty.');
			}
		}
		else {
			throw new Exceptions\DatabaseConnectionConnectException('There is no database connection established.');
		}
	}

	/**
	 * DatabaseConnection::FetchArrayAll()
	 *
	 * @param mixed $Query
	 * @return
	 */
	public function FetchArrayAll($Query) {
		if ($this->IsConnected()) {
			if ((isset($Query)) && (!empty($Query))) {
				try {
					$Result = array();
					if ($this->Rows($Query) > 0) {
						$i = 0;
						while ($Res = $this->FetchArray($Query)) {
							$Result['item_' . $i] = $Res;
							$i++;
						}
						return $Result;
					}
				}
				catch (Exception $Exception) { // ?
					throw $Exception;
				}
			}
			else {
				throw new Exceptions\DatabaseConnectionFetchException('Given SQL query is empty.');
			}
		}
		else {
			throw new Exceptions\DatabaseConnectionConnectException('There is no database connection established.');
		}
	}
}
?>