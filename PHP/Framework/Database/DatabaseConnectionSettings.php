<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Database
 */
namespace Framework\Database;

use \Framework\Root\RootObject;
use \Framework\Database\Exceptions as Exceptions;

/**
 * Encapsulates database connection settings.
 */
class DatabaseConnectionSettings extends RootObject {

	/**
	 * Holds hostname being used.
	 * @var string
	 * @access private
	 */
	private $Host;

	/**
	 * Holds port being used.
	 * @var int
	 * @access private
	 */
	private $Port;

	/**
	 * Holds username being used.
	 * @var string
	 * @access private
	 */
	private $User;

	/**
	 * Holds password being used.
	 * @var string
	 * @access private
	 */
	private $Password;

	/**
	 * Holds database name being used.
	 * @var string
	 * @access private
	 */
	private $Database;

	/**
	 * Created instance of database connection settings class.
	 * If 5 parameters are passed, each of them is fed to {@link \Framework\Database\DatabaseConnectionSettings::SetHost()}, {@link \Framework\Database\DatabaseConnectionSettings::SetPort()}, {@link \Framework\Database\DatabaseConnectionSettings::SetUser()}, {@link \Framework\Database\DatabaseConnectionSettings::SetPassword()} and  {@link \Framework\Database\DatabaseConnectionSettings::SetDatabase()} respectively.
	 * Otherwise, does not call any methods and simply returns {@link \Framework\Database\DatabaseConnectionSettings} instance.
	 * @access public
	 * @param string $Host (Optional)
	 * @param string $Port (Optional)
	 * @param string $User (Optional)
	 * @param string $Password (Optional)
	 * @param string $Database (Optional)
	 * @uses \Framework\Database\DatabaseConnectionSettings::SetHost()
	 * @uses \Framework\Database\DatabaseConnectionSettings::SetPort()
	 * @uses \Framework\Database\DatabaseConnectionSettings::SetUser()
	 * @uses \Framework\Database\DatabaseConnectionSettings::SetPassword()
	 * @uses \Framework\Database\DatabaseConnectionSettings::SetDatabase()
	 * @return \Framework\Database\DatabaseConnectionSettings
	 * @see \Framework\Database\DatabaseConnectionSettings::GetHost()
	 * @see \Framework\Database\DatabaseConnectionSettings::GetPort()
	 * @see \Framework\Database\DatabaseConnectionSettings::GetUser()
	 * @see \Framework\Database\DatabaseConnectionSettings::GetPassword()
	 * @see \Framework\Database\DatabaseConnectionSettings::GetDatabase()
	 */
	public function __construct() {
		if (func_num_args() == 5) {
			$this->SetHost(func_get_arg(0));
			$this->SetPort(func_get_arg(1));
			$this->SetUser(func_get_arg(2));
			$this->SetPassword(func_get_arg(3));
			$this->SetDatabase(func_get_arg(4));
		}
	}

	/**
	 * Returns database connection string.
	 * Returns stringified connection, for example: user:password@host:port/database
	 * @access public
	 * @uses \Framework\Database\DatabaseConnectionSettings::GetHost()
	 * @uses \Framework\Database\DatabaseConnectionSettings::GetPort()
	 * @uses \Framework\Database\DatabaseConnectionSettings::GetUser()
	 * @uses \Framework\Database\DatabaseConnectionSettings::GetPassword()
	 * @uses \Framework\Database\DatabaseConnectionSettings::GetDatabase()
	 * @return string
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionSettingsUserException if given value was invalid.
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionSettingsPasswordException if password was not set before.
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionSettingsHostException if hostname was not set before.
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionSettingsPortException if port was not set before.
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionSettingsDatabaseException if database was not set before.
	 * @see \Framework\Database\DatabaseConnectionSettings::GetHost()
	 * @see \Framework\Database\DatabaseConnectionSettings::GetPort()
	 * @see \Framework\Database\DatabaseConnectionSettings::GetUser()
	 * @see \Framework\Database\DatabaseConnectionSettings::GetPassword()
	 * @see \Framework\Database\DatabaseConnectionSettings::GetDatabase()
	 */
	public function toString() {
		return $this->GetUser() . ':' . $this->GetPassword() . '@' . $this->GetHost() . ':' . $this->GetPort() . '/' . $this->GetDatabase();
	}

	/**
	 * Sets hostname to use.
	 * Sets hostname to given value and returns true or throws an exception if given value was invalid.
	 * @access public
	 * @param string $Host
	 * @uses \Framework\Database\DatabaseConnectionSettings::$Host
	 * @return bool
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionSettingsHostException if given value was invalid.
	 * @see \Framework\Database\DatabaseConnectionSettings::GetHost()
	 */
	public function SetHost($Host) {
		if ((isset($Host)) && (is_string($Host)) && (!empty($Host))) {
			$this->Host = $Host;
			return true;
		}
		else {
			throw new Exceptions\DatabaseConnectionSettingsHostException('Given host is empty.');
		}
	}

	/**
	 * Gets hostname used.
	 * Returns previously set host or throws an exception if hostname was not set before.
	 * @access public
	 * @uses \Framework\Database\DatabaseConnectionSettings::$Host
	 * @return string
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionSettingsHostException if hostname was not set before.
	 * @see \Framework\Database\DatabaseConnectionSettings::SetHost()
	 */
	public function GetHost() {
		if ((isset($this->Host)) && (is_string($this->Host)) && (!empty($this->Host))) {
			return $this->Host;
		}
		else {
			throw new Exceptions\DatabaseConnectionSettingsHostException('Host is empty.');
		}
	}

	/**
	 * Sets port to use.
	 * Sets port to given value and returns true or throws an exception if given value was invalid.
	 * @access public
	 * @param int $Port
	 * @uses \Framework\Database\DatabaseConnectionSettings::$Port
	 * @return bool
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionSettingsPortException if given value was invalid.
	 * @see \Framework\Database\DatabaseConnectionSettings::GetPort()
	 */
	public function SetPort($Port) {
		if ((isset($Port)) && (is_int($Port)) && (!empty($Port))) {
			$this->Port = $Port;
			return true;
		}
		else {
			throw new Exceptions\DatabaseConnectionSettingsPortException('Given port is empty.');
		}
	}

	/**
	 * Gets port used.
	 * Returns previously set port or throws an exception if port was not set before.
	 * @access public
	 * @uses \Framework\Database\DatabaseConnectionSettings::$Port
	 * @return int
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionSettingsPortException if port was not set before.
	 * @see \Framework\Database\DatabaseConnectionSettings::SetPort()
	 */
	public function GetPort() {
		if ((isset($this->Port)) && (is_int($this->Port)) && (!empty($this->Port))) {
			return $this->Port;
		}
		else {
			throw new Exceptions\DatabaseConnectionSettingsPortException('Port is empty.');
		}
	}

	/**
	 * Sets username to use.
	 * Sets username to given value and returns true or throws an exception if given value was invalid.
	 * @access public
	 * @param string $User
	 * @uses \Framework\Database\DatabaseConnectionSettings::$User
	 * @return bool
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionSettingsUserException if given value was invalid.
	 * @see \Framework\Database\DatabaseConnectionSettings::GetUser()
	 */
	public function SetUser($User) {
		if ((isset($User)) && (is_string($User)) && (!empty($User))) {
			$this->User = $User;
			return true;
		}
		else {
			throw new Exceptions\DatabaseConnectionSettingsUserException('Given user is empty.');
		}
	}

	/**
	 * Gets username used.
	 * Returns previously set username or throws an exception if username was not set before.
	 * @access public
	 * @uses \Framework\Database\DatabaseConnectionSettings::$User
	 * @return string
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionSettingsUserException if username was not set before.
	 * @see \Framework\Database\DatabaseConnectionSettings::SetUser()
	 */
	public function GetUser() {
		if ((isset($this->User)) && (is_string($this->User)) && (!empty($this->User))) {
			return $this->User;
		}
		else {
			throw new Exceptions\DatabaseConnectionSettingsUserException('User is empty.');
		}
	}

	/**
	 * Sets password to use.
	 * Sets password to given value and returns true or throws an exception if given value was invalid.
	 * @access public
	 * @param string $Password
	 * @uses \Framework\Database\DatabaseConnectionSettings::$Password
	 * @return bool
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionSettingsPasswordException if given value was invalid.
	 * @see \Framework\Database\DatabaseConnectionSettings::GetPassword()
	 */
	public function SetPassword($Password) {
		if ((isset($Password)) && (is_string($Password))) {
			$this->Password = $Password;
			return true;
		}
		else {
			throw new Exceptions\DatabaseConnectionSettingsPasswordException('Given password is invalid.');
		}
	}

	/**
	 * Gets password used.
	 * Returns previously set password or throws an exception if password was not set before.
	 * @access public
	 * @uses \Framework\Database\DatabaseConnectionSettings::$Password
	 * @return string
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionSettingsPasswordException if password was not set before.
	 * @see \Framework\Database\DatabaseConnectionSettings::SetPassword()
	 */
	public function GetPassword() {
		if ((isset($this->Password)) && (is_string($this->Password))) {
			return $this->Password;
		}
		else {
			throw new Exceptions\DatabaseConnectionSettingsPasswordException('Password is invalid.');
		}
	}

	/**
	 * Sets database to use.
	 * Sets database to given value and returns true or throws an exception if given value was invalid.
	 * @access public
	 * @param string $Database
	 * @uses \Framework\Database\DatabaseConnectionSettings::$Database
	 * @return bool
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionSettingsDatabaseException if given value was invalid.
	 * @see \Framework\Database\DatabaseConnectionSettings::GetDatabase()
	 */
	public function SetDatabase($Database) {
		if ((isset($Database)) && (is_string($Database)) && (!empty($Database))) {
			$this->Database = $Database;
			return true;
		}
		else {
			throw new Exceptions\DatabaseConnectionSettingsDatabaseException('Given database is empty.');
		}
	}

	/**
	 * Gets database used.
	 * Returns previously set database or throws an exception if database was not set before.
	 * @access public
	 * @uses \Framework\Database\DatabaseConnectionSettings::$Database
	 * @return string
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionSettingsDatabaseException if database was not set before.
	 * @see \Framework\Database\DatabaseConnectionSettings::SetDatabase()
	 */
	public function GetDatabase() {
		if ((isset($this->Database)) && (is_string($this->Database)) && (!empty($this->Database))) {
			return $this->Database;
		}
		else {
			throw new Exceptions\DatabaseConnectionSettingsDatabaseException('Database is empty.');
		}
	}
}
?>