<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework\Database
 */
namespace Framework\Database;

use \Framework\Root\RootObject;
use \Framework\Database\Exceptions as Exceptions;

/**
 * Pool for named database connections.
 */
class DatabaseConnectionManager extends RootObject {

	/**
	 * Array holding database connections indexed by connection name
	 * @var array
	 * @access private
	 */
	private $DatabaseConnections;

	/**
	 * Sets database connection.
	 * Adds database connection to the pool and returns true or throws an exception if given values were invalid in any way.
	 * @access public
	 * @param string $Name
	 * @param \Framework\Database\DatabaseConnection $DatabaseConnection
	 * @uses \Framework\Database\DatabaseConnectionManager::$DatabaseConnections
	 * @return bool
	 * @throws \Framework\Database\Exceptions\DatabaseConnectionManagerException if given values ware invalid in any way.
	 * @see \Framework\Database\DatabaseConnectionManager::GetDatabaseConnection()
	 */
	public function SetDatabaseConnection($Name, DatabaseConnection $DatabaseConnection) {
		if ((isset($Name)) && (is_string($Name)) && (!empty($Name)) && (isset($DatabaseConnection)) && (!empty($DatabaseConnection))) {
			$this->DatabaseConnections[$Name] = $DatabaseConnection;
			return true;
		}
		else {
			throw new Exceptions\DatabaseConnectionManagerException('Given database connection is empty.');
		}
	}

	/**
	 * Gets database connection.
	 * Retrieves database connection from the pool by it's name or throws an exception if given values were invalid in any way or such connection was not set before.
	 * @access public
	 * @param string $Name
	 * @uses \Framework\Database\DatabaseConnectionManager::$DatabaseConnections
	 * @return \Framework\Database\DatabaseConnection
	 * @throws Framework\Database\Exceptions\DatabaseConnectionManagerException if name is empty or such connection instance was not set before.
	 * @see \Framework\Database\DatabaseConnectionManager::SetDatabaseConnection()
	 */
	public function GetDatabaseConnection($Name) {
		if ((isset($Name)) && (is_string($Name)) && (!empty($Name))) {
			if ((isset($this->DatabaseConnections[$Name])) && (!empty($this->DatabaseConnections[$Name]))) {
				return $this->DatabaseConnections[$Name];
			}
			else {
				throw new Exceptions\DatabaseConnectionManagerException('Database connection is empty.');
			}
		}
		else {
			throw new Exceptions\DatabaseConnectionManagerException('Given database connection is empty.');
		}
	}
}
?>