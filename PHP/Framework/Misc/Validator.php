<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Utilities;

use \Framework\Root\RootObject;

/**
 * Validator
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class Validator extends RootObject {
	public static $_Instance = null;
	public $NumericRegexp = '#^[0-9]+$#';
	public $AlphaNumericRegexp = '#^[a-z0-9-_]+$#i';
	public $MailRegexp = '#^[a-z0-9-_]+(?:\.[a-z0-9-_]+)*@(?:[a-z0-9-]+(?:\.[a-z0-9-]+)*(?:\.(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z]{2})))$#i';

	/**
	 * Validator::__construct()
	 *
	 * @return
	 */
	public function __construct() {
		$this->ClearSlashes();
	}

	/**
	 * Validator::GetInstance()
	 *
	 * @return
	 */
	public static function GetInstance() {
		if (is_null(self::$_Instance)) self::$_Instance = new Validator();
		return self::$_Instance;
	}

	/**
	 * Validator::IsNum()
	 *
	 * @param mixed $Value
	 * @return
	 */
	public function IsNum($Value) {
		if ((is_string($Value)) || (is_int($Value))) {
			return (preg_match($this->NumericRegexp, $Value)) ? true : false;
		}
		else {
			return false;
		}
	}

	/**
	 * Validator::IsAlNum()
	 *
	 * @param mixed $Value
	 * @return
	 */
	public function IsAlNum($Value) {
		if ((is_string($Value)) || (is_int($Value))) {
			return (preg_match($this->AlphaNumericRegexp, $Value)) ? true : false;
		}
		else {
			return false;
		}
	}

	/**
	 * Validator::IsEmail()
	 *
	 * @param mixed $Value
	 * @return
	 */
	public function IsEmail($Value) {
		if (is_string($Value)) {
			return (preg_match('#^[a-z0-9-_]+(?:\.[a-z0-9-_]+)*@(?:[a-z0-9-]+(?:\.[a-z0-9-]+)*(?:\.(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z]{2})))$#i', $Value)) ? true : false;
		}
		else {
			return false;
		}
	}

	/**
	 * Validator::Escape()
	 *
	 * @param mixed $Value
	 * @return
	 */
	public function Escape($Value) {
		return htmlspecialchars(trim((string) $Value), ENT_QUOTES, 'UTF-8');
	}

	/**
	 * Validator::Unescape()
	 *
	 * @param mixed $Value
	 * @return
	 */
	public function Unescape($Value) {
		return htmlspecialchars_decode((string) $Value, ENT_QUOTES);
	}

	/**
	 * Validator::StripSlash()
	 *
	 * @param mixed $Value
	 * @return
	 */
	public function StripSlash($Value) {
		$Value = (is_array($Value)) ? array_map(array($this, 'StripSlash'), $Value) : stripslashes($Value);
		return $Value;
	}

	/**
	 * Validator::AddSlash()
	 *
	 * @param mixed $Value
	 * @return
	 */
	public function AddSlash($Value) {
		if (is_array($Value)) {
			$Value = (ini_get('magic_quotes_gpc') == false) ? array_map(array($this, 'AddSlash'), $Value) : $Value;
		}
		else {
			$Value = (ini_get('magic_quotes_gpc') == false) ? addslashes($Value) : $Value;
		}
		return $Value;
	}

	/**
	 * Validator::ClearSlashes()
	 *
	 * @return
	 */
	public function ClearSlashes() {
		$_GET = $this->StripSlash($this->AddSlash($_GET));
		$_POST = $this->StripSlash($this->AddSlash($_POST));
	}
}
?>