<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Misc;

use \Framework\Root\RootObject;

/**
 * Misc
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class Misc extends RootObject {
	public static $_Instance = null;

	/**
	 * Misc::GetInstance()
	 *
	 * @return
	 */
	public static function GetInstance() {
		if (is_null(self::$_Instance)) self::$_Instance = new Misc();
		return self::$_Instance;
	}

	/**
	 * Misc::Redirect()
	 *
	 * @param mixed $Url
	 * @return
	 */
	public function Redirect($Url) {
		header('Location: ' . $Url);
		exit;
	}
}
?>