<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Misc;

use \Framework\Root\RootObject;

/**
 * Meta
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class Meta extends RootObject {
	private $Title;
	private $Description;
	private $Keywords;
	private $Canonical;
	private $Robots;
	private $ReplyTo;

	/**
	 * Meta::__construct()
	 *
	 * @return
	 */
	public function __construct() {

	}

	/**
	 * Meta::SetTitle()
	 *
	 * @param mixed $Title
	 * @return
	 */
	public function SetTitle($Title) {
		$this->Title = htmlspecialchars($Title, ENT_QUOTES, 'UTF-8');
	}

	/**
	 * Meta::GetTitle()
	 *
	 * @return
	 */
	public function GetTitle() {
		return ((isset($this->Title)) && (!empty($this->Title))) ? $this->Title : '';
	}

	/**
	 * Meta::SetDescription()
	 *
	 * @param mixed $Description
	 * @return
	 */
	public function SetDescription($Description) {
		$this->Description = htmlspecialchars($Description, ENT_QUOTES, 'UTF-8');
	}

	/**
	 * Meta::GetDescription()
	 *
	 * @return
	 */
	public function GetDescription() {
		return ((isset($this->Description)) && (!empty($this->Description))) ? $this->Description : '';
	}

	/**
	 * Meta::SetKeywords()
	 *
	 * @param mixed $Keywords
	 * @return
	 */
	public function SetKeywords($Keywords) {
		if (is_array($Keywords)) {
			foreach ($Keywords as $Keyword) {
				$this->SetKeywords($Keyword);
			}
		}
		else {
			$this->Keywords[] = htmlspecialchars($Keywords, ENT_QUOTES, 'UTF-8');
		}
	}

	/**
	 * Meta::GetKeywords()
	 *
	 * @return
	 */
	public function GetKeywords() {
		return ((isset($this->Keywords)) && (!empty($this->Keywords))) ? implode(', ', $this->Keywords) : '';
	}

	/**
	 * Meta::SetCanonical()
	 *
	 * @param mixed $Canonical
	 * @return
	 */
	public function SetCanonical($Canonical) {
		$this->Canonical = htmlspecialchars($Canonical, ENT_QUOTES, 'UTF-8');
	}

	/**
	 * Meta::GetCanonical()
	 *
	 * @return
	 */
	public function GetCanonical() {
		return ((isset($this->Canonical)) && (!empty($this->Canonical))) ? $this->Canonical : '';
	}

	/**
	 * Meta::SetRobots()
	 *
	 * @param mixed $Robots
	 * @return
	 */
	public function SetRobots($Robots) {
		$this->Robots = htmlspecialchars($Robots, ENT_QUOTES, 'UTF-8');
	}

	/**
	 * Meta::GetRobots()
	 *
	 * @return
	 */
	public function GetRobots() {
		return ((isset($this->Robots)) && (!empty($this->Robots))) ? $this->Robots : '';
	}

	/**
	 * Meta::SetReplyTo()
	 *
	 * @param mixed $ReplyTo
	 * @return
	 */
	public function SetReplyTo($ReplyTo) {
		$this->ReplyTo = htmlspecialchars($ReplyTo, ENT_QUOTES, 'UTF-8');
	}

	/**
	 * Meta::GetReplyTo()
	 *
	 * @return
	 */
	public function GetReplyTo() {
		return ((isset($this->ReplyTo)) && (!empty($this->ReplyTo))) ? $this->ReplyTo : '';
	}
}
?>