<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Utilities\Css;

use \Framework\Root\RootObject;
use \Framework\Utilities\Css\Exceptions as Exceptions;

/**
 * Css
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class Css extends RootObject {
	private $CssFiles;
	private $Buffer;

	/**
	 * Css::__construct()
	 *
	 * @return
	 */
	public function __construct() {
		if (func_num_args() > 0) {
			for ($i = 0; $i < func_num_args(); $i++) {
				$this->SetFile(func_get_arg($i));
			}
		}
	}

	/**
	 * Css::SetFile()
	 *
	 * @param mixed $CssFiles
	 * @return
	 */
	public function SetFile($CssFiles) {
		if (isset($CssFiles)) {
			if ((is_array($CssFiles)) && (!empty($CssFiles))) {
				for ($i = 0; $i < count($CssFiles); $i++) {
					$this->SetFile($CssFiles[$i]);
				}
			}
			else if (!empty($CssFiles)) {
				if (file_exists($CssFiles)) {
					$this->CssFiles[] = $CssFiles;
				}
				else {
					throw new Exceptions\CssFileNotFoundException('Given css file not exists: ' . $CssFiles);
				}
			}
			else {
				throw new Exceptions\CssException('Given css file is empty.');
			}
		}
		else {
			throw new Exceptions\CssException('Given css file is empty.');
		}
	}

	/**
	 * Css::Merge()
	 *
	 * @return
	 */
	public function Merge() {
		if (count($this->CssFiles) > 0) {
			for ($i = 0; $i < count($this->CssFiles); $i++) {
				$this->Buffer .= file_get_contents($this->CssFiles[$i]);
			}
		}
		else {
			throw new Exceptions\CssException('Css files are empty.');
		}
	}

	/**
	 * Css::Minify()
	 *
	 * @return
	 */
	public function Minify() {
		if (strlen($this->Buffer) > 0) {
			$this->Buffer = preg_replace('#\/\*(.*?)\*\/#s', '', $this->Buffer);
			$this->Buffer = preg_replace("#([\r|\n])#", '', $this->Buffer);
			$this->Buffer = preg_replace("#([\t]+)#", ' ', $this->Buffer);
			$this->Buffer = preg_replace("#([\s]{2,})#", ' ', $this->Buffer);
			$this->Buffer = preg_replace("#(?:(?:[\s]?)([\{|\}|\:\;\,]{1})(?:[\s]?))#", '\\1', $this->Buffer);
		}
		else {
			throw new Exceptions\CssBufferException('Buffer is empty.');
		}
	}

	/**
	 * Css::Output()
	 *
	 * @return
	 */
	public function Output() {
		if (strlen($this->Buffer) > 0) {
			return $this->Buffer;
		}
		else {
			throw new Exceptions\CssBufferException('Buffer is empty.');
		}
	}
}
?>