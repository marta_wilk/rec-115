<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Utilities\Css\Exceptions;

/**
 * CssFileNotFoundException
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class CssFileNotFoundException extends CssException {

}
?>