<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
namespace Framework\Utilities\Css\Exceptions;

use \Framework\Root\RootException;

/**
 * CssException
 *
 * @package
 * @author Entire
 * @copyright Tomasz J�zwik
 * @version 2014
 * @access public
 */
class CssException extends RootException {

}
?>