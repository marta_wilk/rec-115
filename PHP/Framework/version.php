<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Framework
 */
if (!defined('EF_DIR'))
	/**
	 * Root directory of EF.
	 */
	define('EF_DIR', dirname(__FILE__) . DS);

if (!defined('EF_NAME'))
	/**
	 * Name of EF.
	 */
	define('EF_NAME', 'EntireFramework');

if (!defined('EF_VERSION'))
	/**
	 * Version of EF.
	 */
	define('EF_VERSION', '0.7.0');

if (!defined('EF_WWW'))
	/**
	 * Url(s) of EF.
	 */
	define('EF_WWW', '');

if (!defined('EF_EMAIL'))
	/**
	 * E-mail address(es) of EF.
	 */
	define('EF_EMAIL', '');

if (!defined('EF_DOC_URL'))
	/**
	 * Documentation url of EF.
	 */
	define('EF_DOC_URL', '');
?>