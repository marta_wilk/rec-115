<?php
/**
 * @author Tomasz Jozwik <entire@entire.pl>
 * @package Cms
 */
if (!defined('DS'))
	/**
	 * Shortcut for DIRECTORY_SEPARATOR
	 */
	define('DS', DIRECTORY_SEPARATOR);

if (file_exists(dirname(dirname(__FILE__)) . DS . 'Application' . DS . 'Cms' . DS . 'version.php'))
	require_once dirname(dirname(__FILE__)) . DS . 'Application' . DS . 'Cms' . DS . 'version.php';

if (file_exists(dirname(dirname(__FILE__)) . DS . 'Framework' . DS . 'version.php'))
	require_once dirname(dirname(__FILE__)) . DS . 'Framework' . DS . 'version.php';

if ((defined('EF_DIR')) && (defined('EC_DIR'))) {
	require_once EF_DIR . 'Root' . DS . 'RootObject.php';
	require_once EF_DIR . 'Root' . DS . 'RootException.php';
	require_once EF_DIR . 'Root' . DS . 'RootInterface.php';
	require_once EF_DIR . 'ClassLoad' . DS . 'Exceptions' . DS . 'ClassLoaderException.php';
	require_once EF_DIR . 'ClassLoad' . DS . 'Exceptions' . DS . 'ClassLoaderBasePathException.php';
	require_once EF_DIR . 'ClassLoad' . DS . 'Exceptions' . DS . 'ClassLoaderPathException.php';
	require_once EF_DIR . 'ClassLoad' . DS . 'Exceptions' . DS . 'ClassLoaderExtensionException.php';
	require_once EF_DIR . 'ClassLoad' . DS . 'Exceptions' . DS . 'ClassLoaderClassNotFoundException.php';
	require_once EF_DIR . 'ClassLoad' . DS . 'Exceptions' . DS . 'ClassLoaderManagerException.php';
	require_once EF_DIR . 'ClassLoad' . DS . 'Interfaces' . DS . 'ClassLoaderInterface.php';
	require_once EF_DIR . 'ClassLoad' . DS . 'ClassLoaderManager.php';
	require_once EF_DIR . 'ClassLoad' . DS . 'ClassLoader' . DS . 'ClassLoader.php';
	require_once EF_DIR . 'ClassLoad' . DS . 'ClassLoader' . DS . 'FilesystemClassLoader.php';
	require_once EF_DIR . 'ClassLoad' . DS . 'ClassLoader' . DS . 'NamespaceClassLoader.php';
	require_once EF_DIR . 'ClassLoad' . DS . 'ClassLoader' . DS . 'MultiClassLoader.php';
	require_once EF_DIR . 'Bootstrap' . DS . 'Bootstrap.php';
	require_once EF_DIR . 'Bootstrap' . DS . 'WWWBootstrap.php';
	require_once EC_DIR . 'Bootstrap.php';
	new \Application\Cms\Bootstrap();
} else {
	$Format = "%s is not defined. Check your path carefully.\n";
	$Message = '';
	$Message .= (!defined('EF_DIR')) ? sprintf($Format, 'EF_DIR') : '';
	$Message .= (!defined('EC_DIR')) ? sprintf($Format, 'EC_DIR') : '';
	header('Content-Type: text-plain; charset=UTF-8');
	exit($Message);
}
?>