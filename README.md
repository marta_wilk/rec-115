# HOW TO RUN #

1. Add this line to your hosts file:

127.0.0.1 php.localhost www.php.localhost

2. Add this entry to your vhosts file:

<VirtualHost *:80>
	ServerAdmin tech@entire.pl
	DocumentRoot "X:/www/EntireStudio/Testy/PHP/public/"
	ServerName php.localhost
	ServerAlias www.php.localhost
</VirtualHost>

**Adjust path to match your directory**

3. Adjust database settings in \PHP\Application\Cms\Config.php file:

'Database' => array(
	'Root' => array(
		'Host' =>						'localhost',
		'Port' =>						3306,
		'User' =>						'entire',
		'Password' =>					'entire',
		'Database' =>					'php',
		'AutoConnect' =>				true
	)
),

4. Upload database dump from \PHP\database_sql.sql file

5. Open http://php.localhost in your browser

# WHAT IS MISSING BECAUSE OF LACK OF TIME #

1. Edit contact
2. Data pagination
3. Create contact lacks live JS validation
4. Delete all contacts removes all contacts (technically it's valid because DOC says about removing all visible contacts but since paginaton is missing - all contacts are visible, thus removed)